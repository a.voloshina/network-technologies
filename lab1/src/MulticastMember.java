import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.*;
import java.util.HashMap;
import java.util.Map;

public class MulticastMember {

    private int TIMEOUT = 3000;
    private int TIME_TO_SEND = 1000;
    private int RECV_MSG_SIZE = 10;

    private InetAddress groupAddress;
    private int groupPort;
    private String myPort;
    private DatagramSocket sendSocket;
    private MulticastSocket recvSocket;

    private String aliveMsg= "I'm alive!";
    private HashMap<String, Long> copies = new HashMap<>();

//    private String finishMsg= "I'm finish";

    MulticastMember(String address, int port, String my_port) {
        try {
            groupAddress = InetAddress.getByName(address);
        } catch (UnknownHostException e) {
            System.out.println("Can't make InetAddress from 'address' argument");
            e.printStackTrace();
        }
        groupPort = port;
        myPort = my_port;
    }


    void work(){

        try {
            sendSocket = new DatagramSocket();
        } catch (SocketException e) {
            System.out.println("Can't create new DatagramSocket");
            e.printStackTrace();
        }

        DatagramPacket sendPacket = new DatagramPacket(aliveMsg.getBytes(), aliveMsg.length(),
                groupAddress, groupPort);

        if(!myPort.equals("")){
            try {
                sendSocket.bind(new InetSocketAddress(Integer.getInteger(myPort)));
            } catch (SocketException e) {
                System.out.println("Can't bind to " + myPort + " port");
                e.printStackTrace();
            }
        }

        try {
            recvSocket = new MulticastSocket(groupPort);
        } catch (IOException e) {
            System.out.println("Can't create new MulticastSocket");
            e.printStackTrace();
        }
        try {
            recvSocket.joinGroup(groupAddress);
        } catch (IOException e) {
            System.out.println("Can't join to multicast group "+groupAddress.toString());
            e.printStackTrace();
        }
        try {
            recvSocket.setSoTimeout(TIMEOUT);
        } catch (SocketException e) {
            System.out.println("Can't set timeout to socket");
            e.printStackTrace();
        }

        DatagramPacket recvPacket = new DatagramPacket(new byte[RECV_MSG_SIZE], RECV_MSG_SIZE);       //TODO: ??????

        long startTime;
        long endTime = 0;
        int expRecvMsgLen = RECV_MSG_SIZE;
        while (true){
            try {
                sendSocket.send(sendPacket);
                startTime = System.currentTimeMillis();
                ByteArrayOutputStream recvByteArray = new ByteArrayOutputStream();
                int actRecvMsgLen = 0;
                while(actRecvMsgLen < expRecvMsgLen){
                    recvSocket.receive(recvPacket);
                    recvByteArray.write(recvPacket.getData());
                    actRecvMsgLen += recvPacket.getLength();
                }
                String recvMsg = new String(recvByteArray.toByteArray(), 0, actRecvMsgLen);
                System.out.println("Receive msg: "+ recvMsg);
                endTime = System.currentTimeMillis();
                handleRecvMsg(endTime, recvPacket.getAddress(), recvPacket.getPort(), recvMsg);
                if((endTime - startTime) < 1){
                    try {
                        Thread.sleep(TIME_TO_SEND);
                    } catch (InterruptedException ignore) { }
                }
            }
            catch (SocketTimeoutException e){                                                       //TODO: InterruptedIOException?
                System.out.println("Receive timeout!");
                checkUndeads(endTime);
            }
            catch (IOException e) {
                System.out.println("Something going wrong during sending or receiving");
                e.printStackTrace();
                break;
            }
        }

    }

    private void handleRecvMsg(long time, InetAddress addr, int port, String recvMsg){
        if(recvMsg.equals(aliveMsg)){
            tryToAddMember(addr, port, time);
        }
        else {
            System.out.println("Can't recognise msg");
        }
    }

    private void tryToAddMember(InetAddress addr, int port, long time){
        String key = addr.toString() + "|" + String.valueOf(port);
        if(!copies.containsKey(key)){
            copies.put(key, time);
        } else {
            copies.replace(key, time);
        }
        checkUndeads(time);
    }

    private void checkUndeads(long now){
        for(Map.Entry<String, Long> elem: copies.entrySet()){
            if((now - elem.getValue()) > TIMEOUT){
                copies.remove(elem.getKey());
            }
        }
        printCopies();
    }

    private void printCopies(){
        System.out.println();
        System.out.println("Copies Table:");
        for(Map.Entry<String, Long> elem: copies.entrySet()){
            System.out.println(elem.getKey());
        }
        System.out.println();
    }

}
