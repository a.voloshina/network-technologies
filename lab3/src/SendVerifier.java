import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

public class SendVerifier extends Thread {

    private HashMap<Pair<InetAddress, Integer>, Boolean> receivers;
    private DatagramSocket sendSocket;
    private Neighbours neighbours;
    ByteBuffer msg;
    InetAddress ignoredNeighbourIP;
    Integer ignoredNeighbourPort;

    private int SLEEP_TIME = 3000;

    SendVerifier(Neighbours _neighbours,
                 DatagramSocket _sendSocket,
                 ByteBuffer _msg,
                 InetAddress _ignoredNeighbourIP,
                 Integer _ignoredNeighbourPort) {
        neighbours = _neighbours;
        sendSocket = _sendSocket;
        receivers = new HashMap<>();
        for (Pair<InetAddress, Integer> elem : neighbours.getKeySet()) {
            receivers.put(elem, false);
        }
        msg = _msg;
        ignoredNeighbourIP = _ignoredNeighbourIP;
        ignoredNeighbourPort = _ignoredNeighbourPort;
    }

    @Override
    public void run() {
        handleMessage();
    }

    private void handleMessage() {
        try {
            sendSocket = new DatagramSocket();
        } catch (SocketException e) {
            System.err.println("Can't create new receiving DatagramSocket: " + e.getMessage());
            return;
        }

        while (true) {
            System.out.println("SendVerifier of msg " + new String(msg.array()) + "receivers");
            for (Map.Entry<Pair<InetAddress, Integer>, Boolean> elem : receivers.entrySet()) {
                System.out.println(elem.getKey().getKey() + "|" + elem.getKey().getValue() + " = " + elem.getValue());
            }
            if (!receivers.isEmpty()) {
                for (Pair<InetAddress, Integer> elem : receivers.keySet()) {
                    if (!((elem.getKey() == ignoredNeighbourIP) && (elem.getValue().equals(ignoredNeighbourPort)))) {
                        try {
                            DatagramPacket sendPacket = new DatagramPacket(msg.array(), msg.array().length,
                                    elem.getKey(), elem.getValue());
                            sendSocket.send(sendPacket);
                        } catch (IOException e) {
                            System.err.println("Sending packet \"" + new String(msg.array()) + "\" error: " + e.getMessage());
                        }
                    }
                }

                try {
                    sleep(SLEEP_TIME);
                } catch (InterruptedException e) {
                    System.out.println("Stop handling msg \"" + new String(msg.array()) + "\"");
                    break;
                }

                for (Map.Entry<Pair<InetAddress, Integer>, Boolean> elem : receivers.entrySet()) {
                    if (!elem.getValue()) {
                        neighbours.decrementNeighbourTTL(elem.getKey());
                    }
                }
                receivers.entrySet().removeIf(Map.Entry::getValue);
            } else {
                System.out.println("There are no neighbours to send message \"" + new String(msg.array()) + "\"");
                break;
            }
        }
    }

    public boolean setAck(InetAddress senderIP, Integer senderPort) {
        synchronized (receivers) {
            Pair<InetAddress, Integer> sender = new Pair<>(senderIP, senderPort);
            receivers.put(sender, true);
            return receivers.get(sender);
        }
    }

}
