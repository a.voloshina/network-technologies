import java.net.InetAddress;
import java.util.HashMap;
import java.util.Set;

public class Neighbours {
    private HashMap<Pair<InetAddress, Integer>, Integer> neighbours = new HashMap<>();
    private int MAX_TTL = 10;

    public void addNeighbour(InetAddress neighbourIP, int neighbourPort) {
        synchronized (neighbours) {
            neighbours.put(new Pair(neighbourIP, neighbourPort), MAX_TTL);
        }
    }

    public Set<Pair<InetAddress, Integer>> getKeySet() {
        synchronized (neighbours) {
            return neighbours.keySet();
        }
    }

    public Integer getNeighbourTTL(Pair<InetAddress, Integer> neighbour) {
        synchronized (neighbours) {
            return neighbours.get(neighbour);
        }
    }

    public void decrementNeighbourTTL(Pair<InetAddress, Integer> neighbour) {
        synchronized (neighbours) {
            int newTTL = neighbours.get(neighbour) - 1;
            if (newTTL > 0) {
                neighbours.put(neighbour, newTTL);
            } else {
                neighbours.remove(neighbour);
            }
        }
    }

    public void removeNeighbour(InetAddress neighbourIP, int neighbourPort) {
        synchronized (neighbours) {
            neighbours.remove(new Pair(neighbourIP, neighbourPort));
        }
    }

    public boolean isEmpty(){
        synchronized (neighbours){
            return neighbours.isEmpty();
        }
    }

    public void printNeighbours(){
        synchronized (neighbours) {
            for (Pair<InetAddress, Integer> elem : getKeySet()) {
                System.out.println(elem.getKey().toString() + "|" + elem.getValue());
            }
        }
    }

}
