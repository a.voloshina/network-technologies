import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

public class Node {
    private String nodeName;
    private int lossPercentage;
    private int myPort;
    private String parentIP;
    private int parentPort;
    private Neighbours neighbours;

    public static int UUID_BYTES = 16;
    public static final int TYPE_BYTES = 1;
    public static final byte JOIN = 0;
    public static final byte MSG = 1;
    public static final byte ACK = 2;
    public static final byte EXIT = 3;

    public Node(String _nodeName,
                String _lossPercentage,
                String _myPort,
                String _parentIP,
                String _parentPort) {
        nodeName = _nodeName;
        lossPercentage = Integer.parseInt(_lossPercentage);
        myPort = Integer.parseInt(_myPort);
        parentIP = _parentIP;
        parentPort = Integer.parseInt(_parentPort);
        neighbours = new Neighbours();
    }

    public void start() {
        if ((parentIP != null) && (parentPort != 0)) {
            try {
                neighbours.addNeighbour(InetAddress.getByName(parentIP), parentPort);
            } catch (UnknownHostException e) {
                System.err.println("Can't make InetAddress from getting String: " + parentIP);
                return;
            }
        }
        try {
            DatagramSocket sendSocket = new DatagramSocket();
            ForeignMsgReceiver foreignMsgReceiver = new ForeignMsgReceiver(neighbours, myPort, sendSocket);
            foreignMsgReceiver.start();
            UserMsgReader userMsgReader = new UserMsgReader(nodeName, neighbours, myPort, sendSocket, foreignMsgReceiver);
            userMsgReader.start();
        } catch (SocketException e) {
            System.err.println("Can't create new sending DatagramSocket: " + e.getMessage());
        }
    }

}
