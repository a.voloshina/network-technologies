import java.io.IOException;
import java.net.*;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class ForeignMsgReceiver extends Thread {
    private Neighbours neighbours;
    private DatagramSocket recvSocket;
    private DatagramSocket sendSocket;
    private int port;
    private HashMap<UUID, SendVerifier> verifiers;
    private ArrayList<String> lastMsgList;

    private int RECV_MSG_SIZE = 1024;
    private int LAST_MSG_LIST_SIZE = 15;

    public ForeignMsgReceiver(Neighbours _neighbours, int _port, DatagramSocket _sendSocket) {
        neighbours = _neighbours;
        port = _port;
        sendSocket = _sendSocket;
        verifiers = new HashMap<>();
        lastMsgList = new ArrayList<>(LAST_MSG_LIST_SIZE);
    }

    @Override
    public void run() {
        try {
            recvSocket = new DatagramSocket(port, InetAddress.getByName("192.168.43.49"));
        } catch (SocketException | UnknownHostException e) {
            System.err.println("Can't create new receiving DatagramSocket: " + e.getMessage());
            return;
        }
        DatagramPacket recvPacket = new DatagramPacket(new byte[RECV_MSG_SIZE], RECV_MSG_SIZE);

        ByteBuffer joinAckBufferGUID = ByteBuffer.allocate(Node.UUID_BYTES);
        joinAckBufferGUID.putLong(0);
        joinAckBufferGUID.putLong(0);
        final UUID JOIN_ACK_GUID = UUID.nameUUIDFromBytes(joinAckBufferGUID.array());

        while (true) {
            try {
                recvSocket.receive(recvPacket);
            } catch (IOException e) {
                System.err.println("Receiving packet error: " + e.getMessage());
            }

            InetAddress recvAddress = recvPacket.getAddress();

            UUID msgGUID;
            ByteBuffer recvMsg = ByteBuffer.wrap(recvPacket.getData());
            byte msg_type = recvMsg.get();
            int recvPort = recvMsg.getInt();
            switch (msg_type) {
                case Node.JOIN:
                    neighbours.addNeighbour(recvAddress, recvPort);
                    System.out.println("Add child " + recvAddress.toString() + "|" + recvPort);
                    neighbours.printNeighbours();
                    sendAck(0, 0, recvAddress, recvPort);
                    System.out.println();
                    break;

                case Node.MSG:
                    msgGUID = getGUID(recvMsg);
                    if (verifiers.containsKey(msgGUID)) {
                        break;
                    } else {
                        int msgLength = recvMsg.getInt();
                        int msgOffset = Node.TYPE_BYTES + Integer.BYTES + Node.UUID_BYTES + Integer.BYTES;
                        ByteBuffer msgBody = ByteBuffer.allocate(msgLength);
                        msgBody.put(recvMsg.array(), msgOffset, msgLength);
                        ByteBuffer sendMsg = ByteBuffer.allocate(msgOffset + msgLength);
                        sendMsg.put(Node.MSG);
                        sendMsg.putInt(port);
                        sendMsg.putLong(msgGUID.getMostSignificantBits());
                        sendMsg.putLong(msgGUID.getLeastSignificantBits());
                        sendMsg.putInt(msgLength);
                        sendMsg.put(msgBody);

                        SendVerifier handler = new SendVerifier(neighbours, sendSocket, sendMsg, recvAddress, recvPort);
                        verifiers.put(msgGUID, handler);
                        handler.start();

                        String msgBodyString = new String(msgBody.array());
                        System.out.println("Received message \"" + msgBodyString + "\" with UUID " + msgGUID
                                + " and length " + msgLength + " from " + recvAddress.toString() + "|" + recvPort);
                        sendAck(msgGUID.getMostSignificantBits(), msgGUID.getLeastSignificantBits(), recvAddress, recvPort);

                        lastMsgList.add(msgBodyString);
                        System.out.println();
                    }
                    break;

                case Node.ACK:
                    msgGUID = getGUID(recvMsg);
                    for (Map.Entry<UUID, SendVerifier> elem : verifiers.entrySet()) {
                        if (elem.getValue() != null){
                            System.out.println("Msg with UUID " + elem.getKey() + " have verifier");
                        } else {
                            System.out.println("Msg with UUID " + elem.getKey() + " don't have verifier");
                        }
                    }
                    if ((!verifiers.isEmpty())&&(!msgGUID.equals(JOIN_ACK_GUID))) {
                        System.out.println("Trying to get verifier for msg with GUID " + msgGUID);
                        if(verifiers.get(msgGUID) != null){
                            System.out.println("Verifier for msg with UUID " + msgGUID + " exists");
                            if(verifiers.get(msgGUID).setAck(recvAddress, recvPort)){
                                System.out.println("Ack set");
                            }
                        }
                    }
                    System.out.println("Received ack packet from " + recvAddress.toString() + "|" + recvPort);
                    System.out.println("Ack msg GUID: " + msgGUID);
                    System.out.println();
                    break;

                case Node.EXIT:
                    neighbours.removeNeighbour(recvAddress, recvPort);
                    break;
            }
        }
    }

    public void addVerifier(UUID guid, SendVerifier verifier) {
        synchronized (verifiers) {
            verifiers.put(guid, verifier);
        }
    }

    private UUID getGUID(ByteBuffer buffer) {
        return new UUID(buffer.getLong(), buffer.getLong());
    }

    private void sendAck(long firstUUIDPart, long secondUUIDPart, InetAddress sendAddress, int sendPort) {
        ByteBuffer ackMsg = ByteBuffer.allocate(Node.TYPE_BYTES + Node.UUID_BYTES + Integer.BYTES);
        ackMsg.put(Node.ACK);
        ackMsg.putInt(port);
        ackMsg.putLong(firstUUIDPart);
        ackMsg.putLong(secondUUIDPart);
        ByteBuffer guid = ByteBuffer.allocate(Node.UUID_BYTES);
        guid.putLong(firstUUIDPart);
        guid.putLong(secondUUIDPart);
        System.out.println("Ack msg GUID: " + UUID.nameUUIDFromBytes(guid.array()));
        DatagramPacket ackPacket = new DatagramPacket(ackMsg.array(), ackMsg.array().length,
                sendAddress, sendPort);
        try {
            System.out.println("Sending ack packet to " + sendAddress.toString() + "|" + sendPort);
            sendSocket.send(ackPacket);
        } catch (IOException e) {
            System.err.println("Sending ack packet to " + sendAddress.toString() + "|" +
                    sendPort + " error: " + e.getMessage());
        }
    }

    public void exit(){
        for(SendVerifier elem: verifiers.values()){
            elem.interrupt();
        }
        recvSocket.close();
        interrupt();
    }

}
