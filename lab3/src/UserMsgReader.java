import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.UUID;


public class UserMsgReader extends Thread {

    private Neighbours neighbours;
    private DatagramSocket sendSocket;
    ForeignMsgReceiver receiver;
    String node_name;
    private int port;

    UserMsgReader(String _node_name,
                  Neighbours _neighbours,
                  int _port,
                  DatagramSocket _sendSocket,
                  ForeignMsgReceiver _receiver) {
        neighbours = _neighbours;
        port = _port;
        sendSocket = _sendSocket;
        receiver = _receiver;
        node_name = _node_name;
    }

    @Override
    public void run() {
        System.out.println("I'm " + node_name);
        if (!neighbours.isEmpty()) {
            for (Pair<InetAddress, Integer> elem : neighbours.getKeySet()) {
                InetAddress sendAddress = elem.getKey();
                int sendPort = elem.getValue();
                try {
                    ByteBuffer joinGUID = ByteBuffer.allocate(Node.UUID_BYTES);
                    joinGUID.putLong(0);
                    joinGUID.putLong(0);
                    System.out.println("Join msg GUID: " + UUID.nameUUIDFromBytes(joinGUID.array()));
                    ByteBuffer joinMsg = ByteBuffer.allocate(1 + Node.UUID_BYTES + Integer.BYTES);
                    joinMsg.put(Node.JOIN);
                    joinMsg.putInt(port);
                    joinMsg.put(joinGUID);
                    DatagramPacket joinPacket = new DatagramPacket(joinMsg.array(), joinMsg.array().length,
                            sendAddress, sendPort);
                    System.out.println("Sending join packet to " + sendAddress.toString() + "|" +
                            sendPort);
                    sendSocket.send(joinPacket);
                } catch (IOException e) {
                    System.out.println("Sending join packet to " + sendAddress.toString() + "|" +
                            sendPort + " error: " + e.getMessage());
                }
            }
        } else {
            System.out.println("And I'm root!");
        }

        BufferedReader keyboard = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            try {
                String line = keyboard.readLine();
                if (line.equals("exit")) {
                    keyboard.close();
                    sendExitToAll();
                    receiver.exit();
                    sendSocket.close();
                    break;
                } else {
                    UUID msgGUID = UUID.randomUUID();
                    ByteBuffer guid = ByteBuffer.allocate(Node.UUID_BYTES);
                    guid.putLong(msgGUID.getMostSignificantBits());
                    guid.putLong(msgGUID.getLeastSignificantBits());
//                    guid.position(0);
//                    System.out.println("Send msg guid:");
//                    for(int i = 0; i < Node.UUID_BYTES; i++){
//                        System.out.print(guid.get());
//                    }
//                    System.out.println();

                    byte[] msg = line.getBytes();
                    ByteBuffer sendMsgBuffer = ByteBuffer.allocate(Node.TYPE_BYTES + 2*Integer.BYTES + Node.UUID_BYTES
                            + msg.length);
                    sendMsgBuffer.put(Node.MSG);
                    sendMsgBuffer.putInt(port);
                    sendMsgBuffer.put(guid.array());
                    sendMsgBuffer.putInt(msg.length);
                    sendMsgBuffer.put(msg);
                    SendVerifier verifier = new SendVerifier(neighbours, sendSocket, sendMsgBuffer, null, 0);
                    receiver.addVerifier(msgGUID, verifier);
                    System.out.println("Sending user msg " + new String(msg) + " with GUID " + msgGUID + " and length " +
                            msg.length + " to all neighbours");
                    System.out.println();
                    verifier.start();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void sendExitToAll() {
        for (Pair<InetAddress, Integer> elem : neighbours.getKeySet()) {
            InetAddress sendAddress = elem.getKey();
            int sendPort = elem.getValue();
            byte[] sendMsg = {Node.EXIT};
            DatagramPacket exitPacket = new DatagramPacket(sendMsg, Node.TYPE_BYTES,
                    sendAddress, sendPort);
            try {
                sendSocket.send(exitPacket);
            } catch (IOException e) {
                System.out.println("Sending exit packet to " + sendAddress.toString() + "|" +
                        sendPort + " error: " + e.getMessage());
            }
        }
    }

}
