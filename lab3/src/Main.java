public class Main {

    public static void main(String[] args) {
        if (args.length < 3) {
            System.err.println("Input error: list of program arguments should contains at least 3 elements");
            return;
        }
        Node node;
        if (args.length == 5) {
            node = new Node(args[0], args[1], args[2], args[3], args[4]);
        } else {
            node = new Node(args[0], args[1], args[2], null, "0");
        }
        node.start();

    }

}
