import com.google.gson.Gson
import exceptions.UndefinedServerBehaviorException
import model.*
import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody
import java.util.*

class Client(serverIP: String, serverPort: String) {

    companion object {
        private val JSON = MediaType.parse("application/json; charset=utf-8")
        private const val TIMER_REPEAT_TIME = 5000L
        private const val REQUEST_MESSAGES_COUNT = 50
    }

    private val client = OkHttpClient()
    private val statusHandler = StatusHandler(this)
    private val timer = Timer()

    private var token = ""
    var username = "Anonymous"
    private val gson = Gson()
    private val generalUrl = "http://$serverIP:$serverPort/"
    private val messagesHistory = ArrayList<ListElementMessage>()
    private val usersList = UsersList()
    private val activeUsersList = UsersList()
    var generalOffset = 0
    var generalCount = REQUEST_MESSAGES_COUNT

    fun start() {
        login()
        timer.schedule(UpdateUsersInfo(this), TIMER_REPEAT_TIME, TIMER_REPEAT_TIME)

        while (true) {
            val input = readLine() ?: break
            if (input.contains("/user ")) {
                val split = input.split("/")
                if (split.size > 2) {
                    requestUser(split[2].toInt())
                } else {
                    System.err.println("Wrong command format!")
                }
                continue
            }
            when (input) {
                "/logout" -> {
                    logout()
                    System.exit(0)
                }
                "/list" -> activeUsersList.printUsers()
                else -> sendMessage(input)
            }
        }
    }

    fun login() {
        print("Please, enter your nickname: ")
        username = readLine() ?: return
        println()

        getToken(username)

    }

    fun getToken(nickname: String) {
        val requestType = "login"
        val requestBody = RequestBody.create(JSON, gson.toJson(Login(nickname)))
        val request = Request.Builder()
            .url(generalUrl + requestType)
            .addHeader("Content-Type", "application/json")
            .post(requestBody)
            .build()

        val response = client.newCall(request).execute()
        if (statusHandler.handle(response.code())) {
            val responseBody = response.body()?.string() ?: throw UndefinedServerBehaviorException()
            val user = gson.fromJson(responseBody, User::class.java) ?: throw UndefinedServerBehaviorException()
            token = user.token
        }
    }


    fun logout() {
        val requestType = "logout"
        val requestBody = RequestBody.create(JSON, "")
        val request = Request.Builder()
            .url(generalUrl + requestType)
            .addHeader("Authorization", "Token $token")
            .addHeader("Content-Type", "application/json")
            .post(requestBody)
            .build()

        val response = client.newCall(request).execute()
        statusHandler.handle(response.code())
    }

    fun sendMessage(message: String) {
        val requestType = "messages"
        val requestBody = RequestBody.create(JSON, gson.toJson(RequestMessage(message)))
        val request = Request.Builder()
            .url(generalUrl + requestType)
            .addHeader("Content-Type", "application/json")
            .addHeader("Authorization", "Token $token")
            .post(requestBody)
            .build()

        val response = client.newCall(request).execute()
        statusHandler.handle(response.code())
        val responseBody = response.body()?.string() ?: throw UndefinedServerBehaviorException()
        //val responseMessage = gson.fromJson(responseBody, ResponseMessage::class.java)
    }

    fun requestMessageHistory(offset: Int, count: Int) {
        synchronized(messagesHistory) {
            val localUrl = "messages?" +
                    if ((offset != 0) && (count != 0)) {
                        "offset=$offset&count=$count"
                    } else if (offset != 0) {
                        "offset=$offset"
                    } else if (count != 0) {
                        "count=$count"
                    } else {
                        ""
                    }

            val request = Request.Builder()
                .url(generalUrl + localUrl)
                .addHeader("Authorization", "Token $token")
                .get()
                .build()

            val response = client.newCall(request).execute()
            statusHandler.handle(response.code())
            val responseBody = response.body()?.string() ?: throw UndefinedServerBehaviorException()
            val responseMessagesList = gson.fromJson(responseBody, MessagesList::class.java)
            if (responseMessagesList != null) {
                generalCount = responseMessagesList.size
                if (responseMessagesList.size > 0) {
                    messagesHistory.addAll(responseMessagesList.messages)
                    printMessages()
                } else {
                    generalCount = REQUEST_MESSAGES_COUNT
                }
            } else {
                throw UndefinedServerBehaviorException()
            }
        }
    }

    fun requestUsers() {
        synchronized(usersList) {
            val requestType = "users"
            val request = Request.Builder()
                .url(generalUrl + requestType)
                .addHeader("Authorization", "Token $token")
                .get()
                .build()

            val response = client.newCall(request).execute()
            statusHandler.handle(response.code())
            val responseBody = response.body()?.string()
            val responseUsersList = gson.fromJson(responseBody, UsersList::class.java)
                ?: throw UndefinedServerBehaviorException()
            for (responseUser in responseUsersList) {
                if (!usersList.contains(responseUser)) {
                    usersList.add(responseUser)
                    if (responseUser.online == true) {
                        if(responseUser.username != username){
                            println("->User " + responseUser.username + " connected")
                        }
                    }
                } else if (responseUser.online != usersList.get(usersList.indexOf(responseUser)).online) {
                    when (responseUser.online) {
                        true -> println("'User " + responseUser.username + " connected")
                        false -> println("'User " + responseUser.username + " disconnected")
                        null -> println("'User " + responseUser.username + " lost connection")
                    }
                    usersList.setStatus(usersList.indexOf(responseUser), responseUser.online)
                }
            }
            for (user in usersList) {
                if ((user.online == true) && (!activeUsersList.contains(user))) {
                    activeUsersList.add(user)
                }
            }
        }
    }

    fun requestUser(id: Int) {
        val requestType = "users/$id"
        val request = Request.Builder()
            .url(generalUrl + requestType)
            .addHeader("Authorization", "Token $token")
            .get()
            .build()

        val response = client.newCall(request).execute()
        statusHandler.handle(response.code())
        val responseBody = response.body()?.string()
        val responseUser = gson.fromJson(responseBody, ResponseUser::class.java)
            ?: throw UndefinedServerBehaviorException()
        responseUser.print()
    }

    private fun printMessages() {
        synchronized(messagesHistory) {
            if (generalOffset + generalCount > 0) {
                val messagesHistoryPartList = messagesHistory.subList(generalOffset, generalOffset + generalCount)
                for (message in messagesHistoryPartList) {
                    val username = usersList.getUsernameById(message.author) ?: "Unknown"
                    println("<" + username + ">" + ": " + message.message)
                }
                generalOffset += messagesHistoryPartList.size
            }
            generalCount = REQUEST_MESSAGES_COUNT
        }
    }

}