package model

data class ResponseMessage(private val id: Int,
                           private val message: String)