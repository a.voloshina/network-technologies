package model

data class ListElementMessage(val id: Int,
                              val message: String,
                              val author: Int) {

    fun print() {
        println("{")
        println("   id: $id")
        println("   message: $message")
        println("   author: $author")
        println("}")
    }

}