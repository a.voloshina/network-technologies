package model

import java.util.ArrayList

data class UsersList(private val users: ArrayList<ResponseUser> = ArrayList()) : Iterable<ResponseUser> {
    val size: Int
        get() = users.size

    fun add(user: ResponseUser) {
        users.add(user)
    }

    fun get(index: Int): ResponseUser {
        return users[index]
    }

    fun setStatus(index: Int, status: Boolean?){
        users[index].online = status
    }

    override fun iterator() = users.iterator()

    fun getUsernameById(id: Int): String? {
        for (user in users) {
            if(user.id == id){
                return user.username
            }
        }
        return null
    }

    fun printUsers() {
        println("\"users\": [")
        for (user in users) {
            user.print()
            println()
        }
        println("]")
    }

}