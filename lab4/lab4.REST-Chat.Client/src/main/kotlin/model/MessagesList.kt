package model

import java.util.ArrayList

data class MessagesList(val messages: ArrayList<ListElementMessage>) {

    val size: Int
        get() = messages.size

    fun printMessages() {
        println("\"messages\": [")
        for (message in messages) {
            message.print()
            println()
        }
        println("]")
    }

}