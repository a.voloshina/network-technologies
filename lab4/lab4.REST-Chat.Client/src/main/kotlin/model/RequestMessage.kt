package model

data class RequestMessage(val message: String) {
    fun print() {
        println("{")
        println("   message: $message")
        println("}")
    }
}