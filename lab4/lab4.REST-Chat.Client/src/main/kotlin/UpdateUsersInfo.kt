import java.util.*

class UpdateUsersInfo(private val client: Client): TimerTask() {
    override fun run() {
        client.requestUsers()
        client.requestMessageHistory(client.generalOffset, client.generalCount)
    }
}