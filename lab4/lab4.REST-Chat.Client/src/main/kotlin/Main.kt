fun main(args: Array<String>) {
    if (args.size < 2) {
        System.err.println("Wrong input: Application need 2 argument - server ip & port")
        return
    }

    val serverPort = args[1]
    var serverIP = args[0]

    val client = Client(serverIP, serverPort)
    client.start()
}