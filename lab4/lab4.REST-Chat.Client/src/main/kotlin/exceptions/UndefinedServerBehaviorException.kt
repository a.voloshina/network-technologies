package exceptions

class UndefinedServerBehaviorException: Exception() {
    override val message: String?
        get() = "Unsupported server response"
}