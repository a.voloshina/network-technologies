package ru.nsu.fit.avoloshina.restchat.exceptions

class WrongRequestFormatException: ServerException(ErrorHandler.ErrorType.WRONG_REQUEST_FORMAT)  {
}