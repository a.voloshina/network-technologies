package ru.nsu.fit.avoloshina.restchat.model

import java.util.ArrayList

data class MessagesList(val messages: ArrayList<ListElementMessage> = ArrayList()) {

    val size: Int
        get() = messages.size

    fun printMessages() {
        println("\"messages\"(size = $size): [")
        for (message in messages) {
            message.print()
            println()
        }
        println("]")
    }

}