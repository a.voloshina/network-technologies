package ru.nsu.fit.avoloshina.restchat.handlers

import io.undertow.server.HttpServerExchange
import io.undertow.util.HttpString
import ru.nsu.fit.avoloshina.restchat.exceptions.UnknownTokenException
import ru.nsu.fit.avoloshina.restchat.model.RequestMessage
import ru.nsu.fit.avoloshina.restchat.server.ServerData

class LogoutHandler: HttpHandler() {
    override fun handleRequest(exchange: HttpServerExchange, serverData: ServerData) {
        val f = exchange.requestReceiver.receiveFullBytes { ex, data ->
            val token = checkAuthorizationHeader(exchange)
            if(serverData.checkToken(token)){
                exchange.responseHeaders.add(HttpString("Content-Type"), "application/json")
                exchange.statusCode = ServerData.SUCCESS
                exchange.responseSender.send(gson.toJson(RequestMessage("bye!")))
                println("User logout: ")
                serverData.getUserByToken(token)?.print()
                serverData.logoutUser(token)
            } else {
                throw UnknownTokenException(token)
            }
        }
    }
}