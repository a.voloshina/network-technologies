package ru.nsu.fit.avoloshina.restchat.exceptions

class UnknownUserIdException(private val unknownUserId: Int):
        ServerException(ErrorHandler.ErrorType.UNKNOWN_USER_ID) {
}