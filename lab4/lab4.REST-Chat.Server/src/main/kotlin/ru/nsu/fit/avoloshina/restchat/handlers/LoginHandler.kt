package ru.nsu.fit.avoloshina.restchat.handlers

import io.undertow.server.HttpServerExchange
import io.undertow.util.HttpString
import ru.nsu.fit.avoloshina.restchat.exceptions.BusyUsernameException
import ru.nsu.fit.avoloshina.restchat.model.Username
import ru.nsu.fit.avoloshina.restchat.server.ServerData

class LoginHandler : HttpHandler() {
    override fun handleRequest(exchange: HttpServerExchange, serverData: ServerData) {
        val f = exchange.requestReceiver.receiveFullBytes { ex, data ->
            checkContentTypeHeader(exchange)
            val username = gson.fromJson(String(data), Username::class.java)
            if (serverData.checkNewUsername(username.username)) {
                throw BusyUsernameException()
            } else {
                val user = serverData.addUser(username.username)
                exchange.responseHeaders.add(HttpString("Content-Type"), "application/json")
                exchange.statusCode = ServerData.SUCCESS
                exchange.responseSender.send(gson.toJson(user))
            }
        }
    }
}
