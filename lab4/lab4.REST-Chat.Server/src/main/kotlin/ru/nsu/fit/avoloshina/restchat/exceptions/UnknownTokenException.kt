package ru.nsu.fit.avoloshina.restchat.exceptions

class UnknownTokenException(private val token: String): ServerException(ErrorHandler.ErrorType.UNKNOWN_TOKEN)  {
}