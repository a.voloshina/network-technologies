package ru.nsu.fit.avoloshina.restchat.model

import java.util.ArrayList

data class Users(private val users: ArrayList<User> = ArrayList()) : Iterable<User> {
    val size: Int
        get() = users.size

    fun printUsers() {
        println("\"users\": [")
        for (user in users) {
            user.print()
            println()
        }
        println("]")
    }

    fun add(user: User) {
        users.add(user)
    }

    override fun iterator() = users.iterator()
}