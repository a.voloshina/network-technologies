package ru.nsu.fit.avoloshina.restchat.handlers

import com.google.gson.Gson
import io.undertow.server.HttpServerExchange
import ru.nsu.fit.avoloshina.restchat.exceptions.WrongRequestFormatException
import ru.nsu.fit.avoloshina.restchat.server.ServerData

abstract class HttpHandler {

    val gson = Gson()

    abstract fun handleRequest(exchange: HttpServerExchange, serverData: ServerData)

    fun checkAuthorizationHeader(exchange: HttpServerExchange): String{
        if (exchange.requestHeaders.contains("Authorization")) {
            return exchange.requestHeaders["Authorization"].first?.replace("Token ", "")
                    ?: throw WrongRequestFormatException()
        } else {
            throw WrongRequestFormatException()
        }
    }

    fun checkContentTypeHeader(exchange: HttpServerExchange){
        if (exchange.requestHeaders.contains("Content-Type")) {
            if(!exchange.requestHeaders["Content-Type"].first.contains("application/json")){
                throw WrongRequestFormatException()
            }
        } else {
            throw WrongRequestFormatException()
        }
    }
}