package ru.nsu.fit.avoloshina.restchat.handlers

import io.undertow.server.HttpServerExchange
import io.undertow.util.HttpString
import ru.nsu.fit.avoloshina.restchat.exceptions.UnsupportedMethodException
import ru.nsu.fit.avoloshina.restchat.model.HandlerInfo

class BaseHandler{

    private var handlers = HashMap<HandlerInfo, HttpHandler>()

    init {
        handlers[HandlerInfo("login", HttpString("POST"))] = LoginHandler()
        handlers[HandlerInfo("logout", HttpString("POST"))] = LogoutHandler()
        handlers[HandlerInfo("users", HttpString("GET"))] = GetUsersHandler()
        handlers[HandlerInfo("messages", HttpString("POST"))] = MessageHandler()
        handlers[HandlerInfo("messages", HttpString("GET"))] = GetMessagesHandler()
    }

    fun getHandler(exchange: HttpServerExchange, method: HttpString): HttpHandler{
        val splitPath = exchange.relativePath.split("/")
        //val commandAndParams = splitPath[1].split("?")
        //val command = if(commandAndParams.size > 1) { commandAndParams[0] } else { splitPath[1] }
        val command = splitPath[1]
        return handlers[HandlerInfo(command, method)] ?: throw UnsupportedMethodException(command, method)
    }

    fun checkContentType(){

    }

}