package ru.nsu.fit.avoloshina.restchat.model

class User(val id: Int,
           val username: String,
           var online: Boolean?,
           var token: String) {

    override fun equals(other: Any?): Boolean {
        if(other !is User) {
            return false
        }
        return username == other.username && id == other.id && token == other.token
    }

    override fun hashCode(): Int {
        var result = id
        result = 31 * result + username.hashCode()
        result = 31 * result + token.hashCode()
        return result
    }

    fun print() {
        println("{")
        println("   id: $id,")
        println("   username: $username")
        println("   online: $online")
        println("   token: $token")
        println("}")
    }

}