package ru.nsu.fit.avoloshina.restchat.handlers

import io.undertow.server.HttpServerExchange
import io.undertow.util.HttpString
import ru.nsu.fit.avoloshina.restchat.exceptions.UnknownTokenException
import ru.nsu.fit.avoloshina.restchat.model.RequestMessage
import ru.nsu.fit.avoloshina.restchat.model.ResponseMessage
import ru.nsu.fit.avoloshina.restchat.server.ServerData

class MessageHandler : HttpHandler() {
    override fun handleRequest(exchange: HttpServerExchange, serverData: ServerData) {
        val f = exchange.requestReceiver.receiveFullBytes { ex, data ->
            checkContentTypeHeader(exchange)
            val token = checkAuthorizationHeader(exchange)

            println("User $token send message")

            if (serverData.checkToken(token)) {
                val message = gson.fromJson(String(data), RequestMessage::class.java)

                message.print()

                serverData.addMessage(message, token)

                val senderId = serverData.getUserIdByToken(token) ?: throw UnknownTokenException(token)
                exchange.responseHeaders.add(HttpString("Content-Type"), "application/json")
                exchange.statusCode = ServerData.SUCCESS
                exchange.responseSender.send(gson.toJson(ResponseMessage(senderId, message.message)))
            } else {
                throw UnknownTokenException(token)
            }
        }
    }
}