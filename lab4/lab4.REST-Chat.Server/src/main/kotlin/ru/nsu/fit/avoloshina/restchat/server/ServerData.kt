package ru.nsu.fit.avoloshina.restchat.server

import ru.nsu.fit.avoloshina.restchat.exceptions.UnknownTokenException
import ru.nsu.fit.avoloshina.restchat.model.*
import java.security.SecureRandom
import java.util.*
import java.util.concurrent.ConcurrentHashMap

class ServerData {

    companion object {
        const val SUCCESS = 200
        private const val TIMER_REPEAT_TIME = 1000L
    }

    private val secureRandom = SecureRandom()
    val users = Users()
    val messageHistory = MessagesList()
    val timedMessageHistory = ConcurrentHashMap<String, Long>()
    private val timer = Timer()
    private val responseUsersList = UsersList()

    init {
        timer.schedule(TimeoutClients(this), 0, TIMER_REPEAT_TIME)
    }

    fun addUser(username: String): User{
        val bytes = ByteArray(20)
        secureRandom.nextBytes(bytes)
        val token = bytes.toString()

        for (user in users) {
            if (user.username == username) {
                user.online = true
                user.token = token
                aliveUser(user.token)
                println("User connected again:")
                user.print()
                users.printUsers()
                return user
            }
        }
        val user = User(users.size, username, true, token)
        users.add(user)
        println("Add new user:")
        user.print()
        users.printUsers()
        return user
    }

    fun addMessage(msg: RequestMessage, token: String) {
        aliveUser(token)

        val userID = getUserIdByToken(token) ?: throw UnknownTokenException(token)
        messageHistory.messages.add(ListElementMessage(userID, msg.message, userID))
    }

    fun getMessages(offset: Int, count: Int, token: String): ArrayList<ListElementMessage> {
        aliveUser(token)

        println("messageHistory.size = " + messageHistory.size)
        val end =
                if(offset + count <= messageHistory.size){
                    offset + count
                } else {
                    messageHistory.size
                }
        val messagesHistoryPartList = messageHistory.messages.subList(offset, end)
        val messagesHistoryPartArrayList = ArrayList<ListElementMessage>(messagesHistoryPartList.size)
        messagesHistoryPartArrayList.addAll(messagesHistoryPartList)
        return messagesHistoryPartArrayList
    }

    fun getUsers(token: String): UsersList {
        aliveUser(token)

        for (user in users) {
            val responseUser = ResponseUser(user.id, user.username, user.online)
            if(!responseUsersList.contains(responseUser)){
                responseUsersList.add(responseUser)
            }
        }

        return responseUsersList
    }

    fun logoutUser(token: String) {
        for (user in users) {
            if (user.token == token) {
                println("User" + getUserIdByToken(token) + " logout")
                user.online = false
            }
        }
        users.printUsers()
    }

    fun deleteUser(token: String) {
        for (user in users) {
            if (user.token == token) {
                val id = user.id
                println("Timeout session for user $id")
                user.online = null
            }
        }
        users.printUsers()
    }

    private fun aliveUser(token: String) {
        val curTime = System.currentTimeMillis()
        val user = getUserByToken(token)
        user?.online = true
        timedMessageHistory[token] = curTime
    }

    fun getUserById(id: Int): User? {
        for (user in users) {
            if(user.id == id){
                return user
            }
        }
        return null
    }

    fun getUserIdByToken(token: String): Int? {
        for (user in users) {
            if (user.token == token) {
                return user.id
            }
        }
        return null
    }

    fun getUserStatusByToken(token: String): Boolean? {
        for (user in users) {
            if (user.token == token) {
                return user.online
            }
        }
        return null
    }

    fun getUserByToken(token: String): User? {
        for (user in users) {
            if (user.token == token) {
                return user
            }
        }
        return null
    }

    fun getUserTokenByName(username: String): String? {
        for (user in users) {
            if (user.username == username) {
                return user.token
            }
        }
        return null
    }

    fun checkNewUsername(username: String): Boolean{
        for (user in users) {
            if ((user.username == username) && (user.online == true)) {
                return true
            }
        }
        return false
    }

    fun checkToken(token: String): Boolean{
        for (user in users) {
            if ((user.token == token) && (user.online == true)) {
                return true
            }
        }
        return false
    }

}