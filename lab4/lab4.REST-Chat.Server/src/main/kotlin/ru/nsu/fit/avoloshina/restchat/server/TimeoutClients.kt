package ru.nsu.fit.avoloshina.restchat.server

import java.util.*

class TimeoutClients(private val serverData: ServerData) : TimerTask() {

    companion object {
        const val ALIVE_LIMIT = 60000L
    }

    private val timedMessageHistory = serverData.timedMessageHistory

    override fun run() {

        val curTime = System.currentTimeMillis()
        for (pair in timedMessageHistory) {
            val userOnline = serverData.getUserStatusByToken(pair.key)
            if (((curTime - pair.value) > ALIVE_LIMIT) && (userOnline != null)) {
                serverData.deleteUser(pair.key)
            }
        }
    }
}