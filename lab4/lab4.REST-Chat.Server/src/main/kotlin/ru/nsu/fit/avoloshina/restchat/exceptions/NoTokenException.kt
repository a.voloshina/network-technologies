package ru.nsu.fit.avoloshina.restchat.exceptions

class NoTokenException: ServerException(ErrorHandler.ErrorType.NO_TOKEN)  {
}