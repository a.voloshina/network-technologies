package ru.nsu.fit.avoloshina.restchat.model

data class ListElementMessage(private val id: Int,
                              private val message: String,
                              private val author: Int) {

    fun print() {
        println("{")
        println("   id: $id")
        println("   message: $message")
        println("   author: $author")
        println("}")
    }

}