package ru.nsu.fit.avoloshina.restchat.server

import io.undertow.Undertow
import ru.nsu.fit.avoloshina.restchat.exceptions.ErrorHandler
import ru.nsu.fit.avoloshina.restchat.exceptions.ServerException
import ru.nsu.fit.avoloshina.restchat.handlers.BaseHandler
import java.net.NetworkInterface
import java.util.*

class Server(serverIP: String, serverPort: Int) {
    private val data = ServerData()

    private val server = Undertow.builder()
            .addHttpListener(serverPort, serverIP)
            .setHandler{exchange -> (
                    try {
                        BaseHandler().getHandler(
                                exchange,
                                exchange.requestMethod
                        ).handleRequest(exchange, data)
                    } catch (ex: ServerException){
                        val errorHandler = ErrorHandler(exchange)
                        errorHandler.handleError(ex.code)
                    }
                    )}.build()

    fun start(){
        server.start()
    }

    fun stop(){
        server.stop()
    }

}