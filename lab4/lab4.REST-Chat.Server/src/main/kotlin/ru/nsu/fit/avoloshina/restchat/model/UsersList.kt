package ru.nsu.fit.avoloshina.restchat.model

import java.util.ArrayList

class UsersList(private val users: ArrayList<ResponseUser> = ArrayList()) : Iterable<ResponseUser> {
    val size: Int
        get() = users.size

    fun add(user: ResponseUser) {
        users.add(user)
    }

    override fun iterator() = users.iterator()

    fun printUsers() {
        println("\"users\": [")
        for (user in users) {
            user.print()
            println()
        }
        println("]")
    }

}