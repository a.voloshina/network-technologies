package ru.nsu.fit.avoloshina.restchat.exceptions

class BusyUsernameException : ServerException(ErrorHandler.ErrorType.BUSY_USERNAME)