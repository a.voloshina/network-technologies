package ru.nsu.fit.avoloshina.restchat.handlers

import io.undertow.server.HttpServerExchange
import io.undertow.util.HttpString
import ru.nsu.fit.avoloshina.restchat.exceptions.UnknownTokenException
import ru.nsu.fit.avoloshina.restchat.exceptions.WrongRequestFormatException
import ru.nsu.fit.avoloshina.restchat.model.MessagesList
import ru.nsu.fit.avoloshina.restchat.server.ServerData

class GetMessagesHandler : HttpHandler() {

    companion object {
        private const val OFFSET_MIN = 0
        private const val COUNT_MIN = 10
    }

    override fun handleRequest(exchange: HttpServerExchange, serverData: ServerData) {
        val f = exchange.requestReceiver.receiveFullBytes { _, _ ->
            val token = checkAuthorizationHeader(exchange)

            println("User $token ask for messages")

            if (serverData.checkToken(token)) {
                var offset = try {
                    exchange.queryParameters["offset"]?.first?.toInt() ?: OFFSET_MIN
                } catch (ex: NumberFormatException) {
                    throw WrongRequestFormatException()
                }
                var count = try {
                    exchange.queryParameters["count"]?.first?.toInt() ?: COUNT_MIN
                } catch (ex: NumberFormatException) {
                    throw WrongRequestFormatException()
                }

                offset =
                        when {
                            (offset < 0) || (offset > count) -> OFFSET_MIN
                            offset > serverData.messageHistory.size -> serverData.messageHistory.size
                            else -> offset
                        }

                count =
                        when {
                            count < 0 -> COUNT_MIN
                            count > serverData.messageHistory.size -> serverData.messageHistory.size
                            else -> count
                        }

                println("offset = $offset, count = $count ")
                val messages = MessagesList(serverData.getMessages(offset, count, token))

                messages.printMessages()

                exchange.responseHeaders.add(HttpString("Content-Type"), "application/json")
                exchange.statusCode = ServerData.SUCCESS
                exchange.responseSender.send(gson.toJson(messages))
            } else {
                throw UnknownTokenException(token)
            }
        }
    }
}