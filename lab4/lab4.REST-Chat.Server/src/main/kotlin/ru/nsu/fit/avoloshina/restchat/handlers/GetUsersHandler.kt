package ru.nsu.fit.avoloshina.restchat.handlers

import io.undertow.server.HttpServerExchange
import io.undertow.util.HttpString
import ru.nsu.fit.avoloshina.restchat.exceptions.UnknownTokenException
import ru.nsu.fit.avoloshina.restchat.exceptions.UnknownUserIdException
import ru.nsu.fit.avoloshina.restchat.exceptions.WrongRequestFormatException
import ru.nsu.fit.avoloshina.restchat.server.ServerData

class GetUsersHandler : HttpHandler() {
    override fun handleRequest(exchange: HttpServerExchange, serverData: ServerData) {
        val f = exchange.requestReceiver.receiveFullBytes { _, _ ->
                        val token = checkAuthorizationHeader(exchange)
            print("User $token ask for user")
                        if(serverData.checkToken(token)){
                            val splitPath = exchange.relativePath.split("/")
                            exchange.responseHeaders.add(HttpString("Content-Type"), "application/json")
                            exchange.statusCode = ServerData.SUCCESS
                            if (splitPath.size <= 2) {
                                val usersList = serverData.getUsers(token)
                                println("s list:")
                                usersList.printUsers()
                                exchange.responseSender.send(gson.toJson(usersList))
                            }
                            else {
                                val userID = try {
                                    splitPath[2].toInt()
                                } catch (ex: NumberFormatException) {
                                    throw WrongRequestFormatException()
                                }
                                val user = serverData.getUserById(userID)
                                if(user != null){
                                    println(" $userID:")
                                    user.print()
                                    exchange.responseSender.send(gson.toJson(user))
                                } else {
                                    println()
                                    throw UnknownUserIdException(userID)
                                }
                            }
                        } else {
                            println()
                            throw UnknownTokenException(token)
                        }
        }
    }
}