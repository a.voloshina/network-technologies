package ru.nsu.fit.avoloshina.restchat.model

data class RequestMessage(val message: String) {
    fun print() {
        println("{")
        println("   message: $message")
        println("}")
    }
}