package ru.nsu.fit.avoloshina.restchat

import ru.nsu.fit.avoloshina.restchat.server.Server
import java.net.NetworkInterface
import java.util.*

fun main(args: Array<String>) {

    println(getIPAddress())

    if (args.isEmpty()) {
        System.err.println("Wrong input: Application need at least 1 argument - server port")
        return
    }

    val serverPort = args[0].toInt()
    var serverIP = getIPAddress()

    if (args.size == 2) {
        serverIP = args[1]
    }

    val server = Server(serverIP, serverPort)
    server.start()
}

fun getIPAddress(): String {
    var sAddr = ""
    val interfaces = Collections.list(NetworkInterface.getNetworkInterfaces())
    for (intf in interfaces) {
        val addrs = Collections.list(intf.inetAddresses)
        for (addr in addrs) {
            if (!addr.isLoopbackAddress) {
                sAddr = addr.hostAddress
            }
        }
    }
    return sAddr
}