package ru.nsu.fit.avoloshina.restchat.exceptions

open class ServerException(val code: ErrorHandler.ErrorType) : Exception() {
}