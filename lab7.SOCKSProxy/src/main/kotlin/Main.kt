import java.net.InetAddress
import java.net.UnknownHostException

fun main(args: Array<String>) {
    if (args.isEmpty()) {
        System.err.println("Not enough arguments")
        return
    }
//    val address = resolveHostname(args[1])
//    if (address == null){
//        System.err.println("Can't make valid address from " + args[1])
//        return
//    } else {
//        val forwarder = Forwarder("localhost", args[0].toInt(), args[1], args[2].toInt())
//        forwarder.run()
//    }
    val proxy = Proxy("localhost", args[0].toInt())
    proxy.run()

}

fun resolveHostname(hostName: String): InetAddress? {
    return try {
        InetAddress.getByName(hostName)
    } catch (ex: UnknownHostException) {
        null
    }
}