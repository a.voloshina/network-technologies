import java.io.*;
import java.net.*;
import java.nio.ByteBuffer;

public class Client {

    private String filePath;
    private String serverAddr;
    private int serverPort;
    private int SEND_MSG_SIZE = 4096;
    private int TIMEOUT = 3000;

    public Client(String path, String addr, int port) {
        try {
            filePath = new String(path.getBytes(), "UTF-8");
            serverAddr = addr;
            serverPort = port;
        } catch (UnsupportedEncodingException e) {
            System.out.println("Input file path should be in UTF-8 charset");
            e.printStackTrace();
        }
    }

    public void start(){
        try (FileInputStream file = new FileInputStream(filePath);
             Socket socket = new Socket(InetAddress.getByName(serverAddr), serverPort);         //"localhost"
             InputStream sin = socket.getInputStream();
             OutputStream sout = socket.getOutputStream();
             DataInputStream in = new DataInputStream(sin);
             DataOutputStream out = new DataOutputStream(sout)) {

            socket.setSoTimeout(TIMEOUT);
            File f = new File(filePath);
            long fileLen = f.length();

            byte[] byteFileName = f.getName().getBytes();


            ByteBuffer header = ByteBuffer.allocate(byteFileName.length+1+Long.BYTES);
            header.put(byteFileName);
            byte divider = 42;
            header.put(divider);
            header.putLong(fileLen);

            out.write(header.array(), 0, header.array().length);

            byte[] sendBuffer = new byte[SEND_MSG_SIZE];
            long lastPerc = 0;
            long curFileLen = 0;
            System.out.println("Send file: " + f.getName() + (char)divider + fileLen);
            int recvLen;
            while((recvLen = file.read(sendBuffer, 0, SEND_MSG_SIZE)) > 0){
                out.write(sendBuffer, 0, recvLen);
                out.flush();
                curFileLen += recvLen;
                long curPerc = curFileLen*100/fileLen;
                if(curPerc - lastPerc >= 5){
                    System.out.println(curPerc + "% received");
                    lastPerc = curPerc;
                }
            }

            byte isCommunicationSuccessful = in.readByte();
            if (isCommunicationSuccessful == 1){
                System.out.println("File transmission was successful.");
            } else {
                System.out.println("File transmission wasn't successful.");
            }

        } catch (FileNotFoundException e){
            System.err.println("Can't find file " + filePath);
            e.printStackTrace();
        } catch (SocketTimeoutException e) {
            System.err.println("Server agreement reception timeout! File transmission wasn't successful.");
        } catch(IOException e){
            System.err.println("Connection problems: " + e.getMessage() + ". File transmission wasn't successful.");
        }
    }

    public static void main(String[] args) {
        if(args.length < 3){
            System.out.println("Invalid output. Program must have three arguments.");
            return;
        }
        Client client = new Client(args[0], args[1], Integer.parseInt(args[2]));
        client.start();
    }

}
