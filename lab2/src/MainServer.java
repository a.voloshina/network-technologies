import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class MainServer {
    public static void main(String[] args) {
        if(args.length < 1){
            System.out.println("Invalid output: there are should be at least 1 argument");
        }

        int port = Integer.parseInt(args[0]);
        GeneralServer generalServer = new GeneralServer(port);
        generalServer.start();

        BufferedReader keyboard = new BufferedReader(new InputStreamReader(System.in));
        while (true){
            try {
                String line = keyboard.readLine();
                if (line.equals("exit")){
                   generalServer.endWork();
                   break;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
