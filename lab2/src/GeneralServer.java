import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

public class GeneralServer extends Thread{

    private int port;
    private ServerSocket ss;

    public GeneralServer(int port) {
        this.port = port;
    }

    @Override
    public void run(){
        try {
            ss = new ServerSocket(port);
            System.out.println("Waiting for clients");
            while(true){
                ParticularServer particularServer = new ParticularServer(ss.accept());
                particularServer.start();
            }
        }
        catch (SocketException ignored){ }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void endWork(){
        this.interrupt();
        try {
            ss.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
