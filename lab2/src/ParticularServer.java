import java.io.*;
import java.net.Socket;
import java.nio.ByteBuffer;

public class ParticularServer extends Thread{

    private Socket socket;
    private int RECV_MSG_SIZE = 4096;
    private int MAX_FILE_NAME_LEN = 4096;
    private long MAX_FILE_SIZE = 1099511627776L;
    private Long curRecvFileSize = 0L;

    public ParticularServer(Socket _socket){
        socket = _socket;
    }

    @Override
    public void run() {
        try (InputStream sin = socket.getInputStream();
             OutputStream sout = socket.getOutputStream();
             DataInputStream in = new DataInputStream(sin);
             DataOutputStream out = new DataOutputStream(sout)) {


            byte[] readBuffer = new byte[RECV_MSG_SIZE];
            byte divider = 42;
            ByteBuffer fileNameBuffer = ByteBuffer.allocate(MAX_FILE_NAME_LEN);
            ByteBuffer fileSizeBuffer = ByteBuffer.allocate(Long.BYTES);

            String fileName;
            long fileSize;
            FileOutputStream fileOutputStream;
            File file;

            int curFileNameLength = 0;
            long curFileSize = 0;
            System.out.println();
            System.out.println("Receiving file from: " + socket.getInetAddress().toString() + "|" + socket.getPort());

            Speeder speedometer = new Speeder(System.currentTimeMillis(), this);
            speedometer.start();

            while(true){
                byte b = in.readByte();
                if (b != divider) {
                    fileNameBuffer.put(b);
                    curFileNameLength++;
                    curRecvFileSize++;
                    if(curFileNameLength > MAX_FILE_NAME_LEN){
                        System.err.println("Error: File name length should be less then " + MAX_FILE_NAME_LEN + "bytes.");
                        socket.close();
                        return;
                    }
                } else {
                    fileName = new String(fileNameBuffer.array(), 0, curFileNameLength, "UTF-8");
                    System.out.println("Received file name: " + fileName);

                    File dir = new File("./uploads");
                    if (!dir.exists()) {
                        if (dir.mkdir()) {
                            System.out.println("Directory is created!");
                        } else {
                            System.out.println("Can't create directory!");
                        }
                    }
                    String filePath = "./uploads/" + fileName;
                    file = new File(filePath);
                    if(file.exists()){
                        System.err.println("File \"" + fileName + "\" already exists in ./uploads. It will be deleted.");
                        if (!file.delete()){
                            System.err.println("Error: File " + fileName + " can't be deleted from \"./uploads\"");
                            socket.close();
                            return;
                        }
                    }
                    if (!file.createNewFile()){
                        System.err.println("Error: Can't create file " + fileName + " in \"./uploads\"");
                        socket.close();
                        return;
                    }
                    fileOutputStream = new FileOutputStream(filePath);
                    break;
                }

            }

            for(int counter = 0; counter < Long.BYTES; counter++){
                byte b = in.readByte();
                fileSizeBuffer.put(b);
                curRecvFileSize++;
            }

            fileSizeBuffer.position(0);
            fileSize = fileSizeBuffer.getLong();
            System.out.println("Received file size: " + fileSize);

            long lastPerc = 0;
            do {
                int len = in.read(readBuffer, 0, readBuffer.length);
                fileOutputStream.write(readBuffer, 0, len);
                curFileSize += len;
                updateCurRecvFileSize(len);
                long curPerc = curFileSize*100/fileSize;
                if(curPerc - lastPerc>= 5) {
                    System.out.println(curPerc + "% received");
                    lastPerc = curPerc;
                }
            } while (curFileSize < fileSize);

            speedometer.interrupt();
            out.writeByte(1);

        } catch (IOException e) {
            System.out.println("Connection closed. File transmission wasn't successful.");
            System.out.println(e.getMessage());
            e.printStackTrace();
            e.printStackTrace();
        }

        System.out.println("File transmission was successful.");
        System.out.println();
        try {
            socket.close();
        } catch (IOException e) {
            System.err.println("Can't close socket");
            e.printStackTrace();
        }

    }

    private void updateCurRecvFileSize(long size){
        synchronized (curRecvFileSize){
            curRecvFileSize += size;
        }
    }

    Long getCurRecvFileSize() {
        synchronized (curRecvFileSize){
            return curRecvFileSize;
        }
    }
}
