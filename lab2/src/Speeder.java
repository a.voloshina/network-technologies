public class Speeder extends Thread{

    private long startTime;
    private ParticularServer particularServer;
    private long curRecvFileSize = 0;
    private long instantSize = 0;
    private long lastTime;
    private long lastRecvFileSize = 0;

    public Speeder(long _startTime, ParticularServer _particularServer) {
        startTime = _startTime;
        particularServer = _particularServer;
        lastTime = _startTime;
    }

    @Override
    public void run() {
        while(true) {
            try {
                Thread.sleep(1000);
                curRecvFileSize = particularServer.getCurRecvFileSize();
                long endTime = System.currentTimeMillis();
                countInstantSpeed(endTime);
                countAverageSpeed(endTime);
            } catch (InterruptedException ex) {
                break;
            }
        }

    }

    private void countInstantSpeed(long endTime){
        instantSize = curRecvFileSize - lastRecvFileSize;
        lastRecvFileSize = curRecvFileSize;

        Double byteSpeed = (double)instantSize/((double) (endTime - lastTime)/1000.0);
        System.out.println("----Instant speed: " + (int)(byteSpeed/1024.0) + " KB/s");
        lastTime = endTime;
    }

    private void countAverageSpeed(long endTime){
        System.out.println("----Average speed: " + (int)((curRecvFileSize/1024.0)/((double)(endTime-startTime)/1000.0)) + " KB/s");
    }

}
