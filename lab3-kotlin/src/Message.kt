import java.util.UUID

data class Message(var nodeName: String,
                   var type: Byte,
                   var port: Int,
                   var GUID: UUID = UUID.randomUUID() ?: throw Exception("Can't make random UUID"),
                   var body: String = "") : java.io.Serializable