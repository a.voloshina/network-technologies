fun main(args: Array<String>) {

    if (args.size < 3) {
        System.err.println("Input error: list of program arguments should contains at least 3 elements")
        return
    }
    val node: Node = if (args.size == 5) {
        Node(args[0], args[1], args[2], args[3], args[4])
    } else {
        Node(args[0], args[1], args[2], "", "")
    }
    node.start()

}