import java.io.ByteArrayInputStream
import java.io.ObjectInputStream
import java.net.DatagramSocket
import java.net.DatagramPacket
import java.net.InetAddress
import java.util.*
import kotlin.collections.ArrayList


class ForeignMsgReceiver(private val sender: Sender,
                         private val resender: Resender,
                         private val port: Int,
                         private val lossPercentage: Int,
                         private val nodeName: String,
                         private val neighbours: Neighbours) : Thread() {

    companion object {
        private const val MSG_QUEUE_SIZE = 15
        private const val RECV_MSG_SIZE = 1024
    }


    private var lastReceivedMessage = ArrayList<Message>(MSG_QUEUE_SIZE)

    override fun run() {
        val recvSocket = DatagramSocket(port)

        while (true) {
            if(isInterrupted){
                println("Last received messages: ")
                for(message in lastReceivedMessage){
                    println(message.nodeName + ": " + message.body)
                }
                recvSocket.close()
                interrupt()
            }
            val recvPacket = DatagramPacket(ByteArray(RECV_MSG_SIZE), RECV_MSG_SIZE)
            recvSocket.receive(recvPacket)
            val r = Random()
            if (r.nextInt(100) < lossPercentage) {
                continue
            }

            val byteArrayInputStream = ByteArrayInputStream(recvPacket.data)
            val objectInputStream = ObjectInputStream(byteArrayInputStream)
            val message = objectInputStream.readObject() as Message

            when (message.type) {
                Node.MSG -> handleMessage(message, recvPacket.address)
                Node.ACK -> handleAck(message, recvPacket.address)
                Node.EXIT -> handleExit(message, recvPacket.address)
            }

            objectInputStream.close()
            byteArrayInputStream.close()
        }
    }

    private fun handleMessage(_message: Message, senderIP: InetAddress) {
        val ackMessage = Message(nodeName, Node.ACK, port, _message.GUID, body = "ACK")
        sender.sendMessage(ackMessage, senderIP, _message.port)


        if(Node.DEBUG){
            println("Received message \"" + _message.body + "\" with GUID = " + _message.GUID + " from " + _message.nodeName
                    + "(" + senderIP + "|" + _message.port + ")")
        } else {
            println(_message.nodeName + ": " + _message.body)
        }

        neighbours.addNeighbour(senderIP, _message.port)


        var isThisMessageAlreadyReceived = false
        for(message in lastReceivedMessage){
            if(_message.GUID == message.GUID){
                isThisMessageAlreadyReceived = true
                break
            }
        }


        if (!isThisMessageAlreadyReceived){
            val message = Message(_message.nodeName, _message.type, port, _message.GUID, _message.body)
            sender.sendToAll(message, senderIP, _message.port)
            resender.addReceiversFor(message, senderIP, _message.port)
        }

        if(lastReceivedMessage.size == MSG_QUEUE_SIZE){
            lastReceivedMessage.removeAt(0)
        }
        lastReceivedMessage.add(_message)
    }

    private fun handleAck(message: Message, senderIP: InetAddress) {
        println("Received ack message with GUID = " + message.GUID + " from " + message.nodeName)
        resender.removeReceiverFor(message, senderIP, message.port)
    }

    private fun handleExit(message: Message, senderIP: InetAddress) {
        println("Received exit message from " + message.nodeName)
        sender.sendToAll(message, senderIP, message.port)
        neighbours.removeNeighbour(senderIP, message.port)
    }

}