import java.net.InetAddress
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.LinkedBlockingQueue
import kotlin.collections.HashMap

class Resender(private val sender: Sender,
               private val neighbours: Neighbours) : Thread() {

    private val notConfirmedMessages = ConcurrentHashMap<Message, LinkedBlockingQueue<Pair<InetAddress, Int>>>()

    companion object {
        private const val TIME_TO_SLEEP = 1000L
    }

    fun addReceiversFor(message: Message, ignoredNeighbourAddress: InetAddress?, ignoredNeighbourPort: Int?) {
        val expectedForAckNeighbours = LinkedBlockingQueue<Pair<InetAddress, Int>>()
        for (neighbour in neighbours.getKeySet()) {
            if ((neighbour.first != ignoredNeighbourAddress) || (neighbour.second != ignoredNeighbourPort)) {
                expectedForAckNeighbours.put(neighbour)
            }
        }
        notConfirmedMessages[message] = expectedForAckNeighbours
        printNotConfirmedMessages()
    }

    fun removeReceiverFor(message: Message, expectedForAckNeighbourAddress: InetAddress, expectedForAckNeighbourPort: Int){
        val it = notConfirmedMessages.entries.iterator()
        while (it.hasNext()) {
            val elem = it.next()
            if(elem.key.GUID == message.GUID){
                val iterator = elem.value.iterator()
                while (iterator.hasNext()) {
                    val resender = iterator.next()
                    if((resender.first == expectedForAckNeighbourAddress)&&(resender.second == expectedForAckNeighbourPort)){
                        iterator.remove()
                        return
                    }
                }
            }
        }
        printNotConfirmedMessages()
    }

    private fun printNotConfirmedMessages(){
        if(Node.DEBUG){
            println("-------------------NotConfirmedMessages---------------------------")
        }
        for(elem in notConfirmedMessages){
            if(Node.DEBUG){
                println("Message \"" + elem.key.body + "\"" + " receivers: ")
            }
            for(receiver in elem.value){
                if(Node.DEBUG){
                    println("->" + receiver.first.toString() + "|" + receiver.second)
                }
            }
        }
        if(Node.DEBUG){
            println("------------------------------------------------------------------")
        }
    }

    override fun run() {
        while (true){

            try {
                sleep(TIME_TO_SLEEP)
            } catch (ex: InterruptedException){
                interrupt()
            }


            val isEvenOnceConfirmedReceivers = HashMap<Pair<InetAddress, Int>, Boolean>()
            for(neighbour in neighbours.getKeySet()){
                isEvenOnceConfirmedReceivers[neighbour] = notConfirmedMessages.isEmpty()
            }

            val iter = notConfirmedMessages.entries.iterator()
            while (iter.hasNext()) {
                val messageQueuePair = iter.next()
                if (messageQueuePair.value.isEmpty()) {
                    iter.remove()
                    continue
                } else {
                    val iterator = messageQueuePair.value.iterator()
                    while (iterator.hasNext()) {
                        val receiverPair = iterator.next()
                        if (!neighbours.containsKey(receiverPair.first, receiverPair.second)) {
                            iterator.remove()
                        } else {
                            println(receiverPair.first.toString() + "|" + receiverPair.second + " lost message \"" +
                            messageQueuePair.key + "\"")
                        }
                    }
                }

                for(receiver in isEvenOnceConfirmedReceivers){
                    if (!messageQueuePair.value.contains(receiver.key)){
                        isEvenOnceConfirmedReceivers[receiver.key] = true
                    }
                }
            }

            for (receiver in isEvenOnceConfirmedReceivers){
                if(!receiver.value){
                    neighbours.decrementNeighbourTTL(receiver.key.first, receiver.key.second)
                } else {
                    neighbours.turnNeighbourTTLup(receiver.key.first, receiver.key.second)
                }
            }

            for(elem in notConfirmedMessages){
                for(receiver in elem.value){
                    sender.sendMessage(elem.key, receiver.first, receiver.second)
                }
            }
        }
    }

}
