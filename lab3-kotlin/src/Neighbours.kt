import java.net.InetAddress

class Neighbours {
    private var neighbours = HashMap<Pair<InetAddress, Int>, Int>()

    companion object {
        private const val MAX_TTL = 15
    }

    //TODO: understand & use ConcurrentHashMap

    fun addNeighbour(neighbourIP: InetAddress, neighbourPort: Int){
        synchronized(neighbours){
            if(!containsKey(neighbourIP, neighbourPort)){
                neighbours[Pair(neighbourIP, neighbourPort)] = MAX_TTL
            }
        }
        printNeighbours()
    }

    fun removeNeighbour(neighbourIP: InetAddress, neighbourPort: Int){
        synchronized(neighbours){
            if(containsKey(neighbourIP, neighbourPort)){
                neighbours.remove(findKey(neighbourIP, neighbourPort))
            }
        }
        printNeighbours()
    }

    fun getKeySet(): Set<Pair<InetAddress, Int>> {
        synchronized(neighbours){
            return neighbours.keys
        }
    }

    fun containsKey(neighbourIP: InetAddress, neighbourPort: Int):Boolean {
        synchronized(neighbours){
            for(neighbour in neighbours.keys){
                if((neighbour.first == neighbourIP)&&(neighbour.second == neighbourPort)){
                    return true
                }
            }
            return false
        }
    }

    fun decrementNeighbourTTL(neighbourIP: InetAddress, neighbourPort: Int) {
        synchronized(neighbours) {
            if(!containsKey(neighbourIP, neighbourPort)){
                throw Exception("No such neighbour $neighbourIP|$neighbourPort in Neighbours")
            } else {
                val neighbour = findKey(neighbourIP, neighbourPort)!!
                val newTTL = (neighbours[neighbour])!!.minus(1)
                if (newTTL > 0) {
                    neighbours.put(neighbour, newTTL)
                } else {
                    println("Neighbour " + neighbour.first + "|" + neighbour.second + " dead")
                    neighbours.remove(neighbour)
                }
            }

        }
    }

    fun turnNeighbourTTLup(neighbourIP: InetAddress, neighbourPort: Int){
        synchronized(neighbours){
            if(containsKey(neighbourIP, neighbourPort)){
                val neighbour = findKey(neighbourIP, neighbourPort)!!
                neighbours[neighbour] = MAX_TTL
            }

        }
    }

    private fun findKey(neighbourIP: InetAddress, neighbourPort: Int): Pair<InetAddress, Int>?{
        synchronized(neighbours){
            for(neighbour in neighbours.keys){
                if((neighbour.first == neighbourIP)&&(neighbour.second == neighbourPort)){
                    return neighbour
                }
            }
            return null
        }
    }

    fun printNeighbours() {
        synchronized(neighbours) {
            println("-------------------neighbours---------------------------")
            for (elem in getKeySet()) {
                println(elem.first.toString() + "|" + elem.second)
            }
            println("--------------------------------------------------------")
        }
    }

}
