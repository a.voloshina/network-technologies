import java.net.DatagramSocket
import java.net.InetAddress
import java.net.NetworkInterface
import java.util.*

class Node(private var nodeName: String,
           private var lossPercentage: String,
           private var myPort: String,
           private var parentIP: String,
           private var parentPort: String) : Thread() {

    private var neighbours = Neighbours()
    private val sendSocket = DatagramSocket()

    companion object {
        const val MSG: Byte = 1
        const val ACK: Byte = 2
        const val EXIT: Byte = 3

        const val DEBUG = true

        fun getIPAddress(): String {
            var sAddr = ""
            val interfaces = Collections.list(NetworkInterface.getNetworkInterfaces())
            for (intf in interfaces) {
                val addrs = Collections.list(intf.inetAddresses)
                for (addr in addrs) {
                    if (!addr.isLoopbackAddress) {
                        sAddr = addr.hostAddress
                    }
                }
            }
            return sAddr
        }

    }

    override fun run() {

        println("I'm: " + nodeName + "(" + Node.getIPAddress() + "|" + myPort + ")")

        if (((parentIP != "") && (parentPort != ""))) {
            neighbours.addNeighbour(InetAddress.getByName(parentIP), parentPort.toInt())
        }

        val sender = Sender(sendSocket, neighbours)
        val resender = Resender(sender, neighbours)
        val receiver = ForeignMsgReceiver(sender, resender, myPort.toInt(), lossPercentage.toInt(), nodeName,
                neighbours)
        receiver.start()
        sender.start()
        resender.start()

        work(sender, resender, receiver)
    }

    private fun work(sender: Sender, resender: Resender, receiver: ForeignMsgReceiver) {
        while (true) {
            val input = readLine() ?: break
            if (input == "exit") {
                sendExit(sender)
                resender.interrupt()
                receiver.interrupt()
                sender.interrupt()
                sendSocket.close()
                break
            }
            if (input == "neighbours"){
                neighbours.printNeighbours()
                continue
            }

            val message = Message(nodeName, MSG, myPort.toInt(), body = input)
            sender.sendToAll(message, null, null)
            resender.addReceiversFor(message, null, null)
        }
    }

    fun sendExit(sender: Sender) {
        val exitMessage = Message(nodeName, EXIT, myPort.toInt())
        sender.sendToAll(exitMessage, null, null)
    }

}
