import java.io.ByteArrayOutputStream
import java.io.ObjectOutputStream
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetAddress
import java.util.concurrent.LinkedBlockingQueue

class Sender(private val sendSocket: DatagramSocket,
             private val neighbours: Neighbours) : Thread() {

    private var messages = LinkedBlockingQueue<Pair<Pair<InetAddress, Int>, Message>>()

    override fun run() {
        while (true) {
            val messageElem = messages.take()

            if(isInterrupted){
                interrupt()
            }

            val message = messageElem.second

            val byteArrayOutputStream = ByteArrayOutputStream()
            val objectOutputStream = ObjectOutputStream(byteArrayOutputStream)
            objectOutputStream.writeObject(message)
            objectOutputStream.flush()
            objectOutputStream.close()

            val sendAddress = messageElem.first.first
            val sendPort = messageElem.first.second

            val sendPacket = DatagramPacket(byteArrayOutputStream.toByteArray(), byteArrayOutputStream.size(),
                    sendAddress, sendPort)
            sendSocket.send(sendPacket)

            byteArrayOutputStream.close()

            if(Node.DEBUG){
                println("Message \"" + message.body + "\" was sent to " + sendAddress + "|" + sendPort)
            }
        }
    }

    fun sendMessage(message: Message, toReplyNeighbourAddress: InetAddress, toReplyNeighbourPort: Int) {
        messages.add(Pair(Pair(toReplyNeighbourAddress, toReplyNeighbourPort), message))
    }

    fun sendToAll(message: Message, ignoredNeighbourAddress: InetAddress?, ignoredNeighbourPort: Int?) {
        if(Node.DEBUG){
            println("Add in queue message \"" + message.body + "\" to all neighbours")
        }
        val set = neighbours.getKeySet()
        for (neighbour in set) {
            if ((neighbour.first != ignoredNeighbourAddress) || (neighbour.second != ignoredNeighbourPort)) {
                if(Node.DEBUG){
                    println("Hop hey lalalei")
                }
                messages.add(Pair(Pair(neighbour.first, neighbour.second), message))
            }
        }
        if(Node.DEBUG){
            println("Adding completed")
        }
    }

}