package ru.nsu.fit.avoloshina.websocketchat.client

import com.google.gson.Gson
import ru.nsu.fit.avoloshina.websocketchat.model.*
import ru.nsu.fit.avoloshina.websocketchat.client.exceptions.*
import okhttp3.*
import java.util.concurrent.CopyOnWriteArrayList

class Client(serverIP: String, serverPort: String) {

    companion object {
        private val JSON = MediaType.parse("application/json; charset=utf-8")
        const val DEBUG = false
    }

    private val client = OkHttpClient()
    private lateinit var websocket: WebSocket
    val statusHandler = StatusHandler(this)

    private var token = ""
    var id = 0
    var username = "Anonymous"
    val gson = Gson()
    private val generalUrl = "http://$serverIP:$serverPort/"
    private val messagesHistory = CopyOnWriteArrayList<ListElementMessage>()
    private val usersList = UsersList()
    private val activeUsersList = UsersList()

    fun start() {                                           //TODO: try for "Server closed connection" exception
        try {
            connect()
            login()

            while (true) {
                val input = readLine() ?: break
                val isCommand = parseCommand(input) ?: return
                if(!isCommand){
                    websocket.send(gson.toJson(ResponseMessage(id, input)))
                }
            }
        } catch (ex: Exception) {
            System.err.println(ex.message)
        }
    }

    private fun parseCommand(input: String): Boolean? {
        if (input.contains("/")) {
            if (input.contains("/user ")) {
                val split = input.split(" ")
                if (split.size > 2) {
                    requestUser(split[2].toInt())
                } else {
                    System.err.println("Wrong command format!")
                }
                return true
            }
            val command = input.split("/")
            return when (command[1]) {
                "/logout" -> {
                    logout()
                    websocket.cancel()
                    null
                }
                "/list" -> {
                    requestUsers()
                    true
                }
                else -> {
                    sendUnknownCommand(command[1])
                    true
                }
            }
        } else {
            return false
        }

    }

    fun login() {
        if(DEBUG){
            println("trying to login")
        }
        print("Please, enter your nickname: ")
        username = readLine() ?: return
        println()

        getToken(username)

    }

    private fun connect() {
        val localUrl = "messages"

        val request = Request.Builder()
            .url(generalUrl + localUrl)
            .addHeader("Authorization", "Token $token")
            .get()
            .build()
        if(DEBUG){
            println("trying to connect server throw websocket")
        }
        websocket = client.newWebSocket(request, ServerListener(this))
    }


    fun getToken(nickname: String) {
        val requestType = "login"
        val requestBody = RequestBody.create(JSON, gson.toJson(Login(nickname)))
        val request = Request.Builder()
            .url(generalUrl + requestType)
            .addHeader("Content-Type", "application/json")
            .post(requestBody)
            .build()

        val response = client.newCall(request).execute()
        if (statusHandler.handle(response.code())) {
            val responseBody = response.body()?.string() ?: throw UndefinedServerBehaviorException()
            if(DEBUG){
                println("token response body = $responseBody")
            }
            val user = gson.fromJson(responseBody, User::class.java) ?: throw UndefinedServerBehaviorException()
            token = user.token
            id = user.id
            if(DEBUG){
                println("login successful")
            }
        }
    }

    private fun logout() {
        val requestType = "logout"
        val requestBody = RequestBody.create(JSON, "")
        val request = Request.Builder()
            .url(generalUrl + requestType)
            .addHeader("Authorization", "Token $token")
            .addHeader("Content-Type", "application/json")
            .post(requestBody)
            .build()

        val response = client.newCall(request).execute()
        statusHandler.handle(response.code())
    }


    private fun requestUsers() {
        val requestType = "users"
        val request = Request.Builder()
            .url(generalUrl + requestType)
            .addHeader("Authorization", "Token $token")
            .get()
            .build()

        val response = client.newCall(request).execute()
        statusHandler.handle(response.code())
        val responseBody = response.body()?.string()
        val responseUsersList = gson.fromJson(responseBody, UsersList::class.java)
            ?: throw UndefinedServerBehaviorException()
        addUsers(responseUsersList)
        activeUsersList.printUsers()
    }

    private fun requestUser(id: Int) {
        val requestType = "users/$id"
        val request = Request.Builder()
            .url(generalUrl + requestType)
            .addHeader("Authorization", "Token $token")
            .get()
            .build()

        val response = client.newCall(request).execute()
        statusHandler.handle(response.code())
        val responseBody = response.body()?.string()
        val responseUser = gson.fromJson(responseBody, ResponseUser::class.java)
            ?: throw UndefinedServerBehaviorException()
        responseUser.print()
    }

    private fun sendUnknownCommand(input: String){
        val request = Request.Builder()
            .url(generalUrl + input)
            .addHeader("Authorization", "Token $token")
            .get()
            .build()
        val response = client.newCall(request).execute()
        statusHandler.handle(response.code())
    }

    fun printMessage(message: ListElementMessage) {
        val username = usersList.getUsernameById(message.author) ?: "Unknown"
        println("<" + username + ">" + ": " + message.message)
    }

    fun addMessage(message: ListElementMessage) {
        messagesHistory.add(message)
    }

    fun addUsers(responseUsersList: UsersList) {
        for (responseUser in responseUsersList) {
            addUser(responseUser)
        }

        updateAliveUsers()
    }

    private fun addUser(responseUser: ResponseUser) {
        synchronized(usersList) {
            if (!usersList.contains(responseUser)) {
                usersList.add(responseUser)
                if (responseUser.online == true) {
                    if (responseUser.username != username) {
                        println("User " + responseUser.username + " connected")
                    }
                }
            } else {
//                if(DEBUG) {
//                    println(
//                        "Know user " + responseUser.username + ":" + responseUser.online
//                                + " with status " + usersList.get(usersList.indexOf(responseUser)).online
//                    )
//                }
                if (responseUser.online != usersList.get(usersList.indexOf(responseUser)).online) {
                    when (responseUser.online) {
                        true -> println("User " + responseUser.username + " connected")
                        false -> println("User " + responseUser.username + " disconnected")
                        null -> println("User " + responseUser.username + " lost connection")
                    }
                    usersList.setStatus(usersList.indexOf(responseUser), responseUser.online)
                }
            }
        }
    }

    private fun updateAliveUsers() {
        synchronized(usersList) {
            for (user in usersList) {
                if ((user.online == true) && (!activeUsersList.contains(user))) {
                    activeUsersList.add(user)
                }
            }
        }
    }

}
