package ru.nsu.fit.avoloshina.websocketchat.server.exceptions


open class ServerException(val code: ErrorHandler.ErrorType) : Exception() {
}
