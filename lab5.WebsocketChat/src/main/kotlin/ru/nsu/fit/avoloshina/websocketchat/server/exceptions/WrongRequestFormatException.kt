package ru.nsu.fit.avoloshina.websocketchat.server.exceptions

class WrongRequestFormatException: ServerException(ErrorHandler.ErrorType.WRONG_REQUEST_FORMAT)