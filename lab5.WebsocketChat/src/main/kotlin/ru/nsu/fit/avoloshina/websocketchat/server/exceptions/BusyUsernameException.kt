package ru.nsu.fit.avoloshina.websocketchat.server.exceptions

class BusyUsernameException : ServerException(ErrorHandler.ErrorType.BUSY_USERNAME)