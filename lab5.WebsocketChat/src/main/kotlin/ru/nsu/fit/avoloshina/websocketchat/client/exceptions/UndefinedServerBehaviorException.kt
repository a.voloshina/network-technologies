package ru.nsu.fit.avoloshina.websocketchat.client.exceptions

class UndefinedServerBehaviorException: Exception() {
    override val message: String?
        get() = "Unsupported server response"
}
