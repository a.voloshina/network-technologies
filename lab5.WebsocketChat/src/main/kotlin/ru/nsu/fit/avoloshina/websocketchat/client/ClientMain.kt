package ru.nsu.fit.avoloshina.websocketchat.client

fun main(args: Array<String>) {
    if (args.size < 2) {
        System.err.println("Wrong input: Application need 2 argument - ru.nsu.fit.avoloshina.websocketchat.server ip & port")
        return
    }

    val serverPort = args[1]
    val serverIP = args[0]

    val client = Client(serverIP, serverPort)
    client.start()
}
