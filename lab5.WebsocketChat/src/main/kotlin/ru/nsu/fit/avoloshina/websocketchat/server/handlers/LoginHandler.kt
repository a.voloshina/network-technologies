package ru.nsu.fit.avoloshina.websocketchat.server.handlers

import io.undertow.server.HttpHandler
import io.undertow.server.HttpServerExchange
import io.undertow.util.HttpString
import ru.nsu.fit.avoloshina.websocketchat.model.Username
import ru.nsu.fit.avoloshina.websocketchat.server.ServerData
import ru.nsu.fit.avoloshina.websocketchat.server.exceptions.ErrorHandler

class LoginHandler(private val serverData: ServerData): HttpHandler {
    override fun handleRequest(exchange: HttpServerExchange) {
        val f = exchange.requestReceiver.receiveFullBytes { _, data ->
            serverData.checkContentTypeHeader(exchange)
            val username = serverData.gson.fromJson(String(data), Username::class.java)
            println("User $username trying to login")
            if (serverData.checkNewUsername(username.username)) {
                ErrorHandler.handleError(ErrorHandler.ErrorType.BUSY_USERNAME, exchange)
            } else {
                val user = serverData.addUser(username.username)
                exchange.responseHeaders.add(HttpString("Content-Type"), "application/json")
                exchange.statusCode = ServerData.SUCCESS
                exchange.responseSender.send(serverData.gson.toJson(user))
                //serverData.usersListener(serverData.getResponseUser(user))
            }
        }
    }
}
