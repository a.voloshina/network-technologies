package ru.nsu.fit.avoloshina.websocketchat.model

import java.util.concurrent.CopyOnWriteArrayList

data class MessagesList(val messages: CopyOnWriteArrayList<ListElementMessage> = CopyOnWriteArrayList()) {

    val size: Int
        get() = messages.size

    fun printMessages() {
        println("\"messages\": [")
        for (message in messages) {
            message.print()
            println()
        }
        println("]")
    }

}
