package ru.nsu.fit.avoloshina.websocketchat.model

data class RequestMessage(val message: String) {
    fun print() {
        println("{")
        println("   message: $message")
        println("}")
    }
}
