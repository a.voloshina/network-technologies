package ru.nsu.fit.avoloshina.websocketchat.model

import io.undertow.util.HttpString

class HandlerInfo(val command: String, val method: HttpString) {

    override fun equals(other: Any?): Boolean {
        other as HandlerInfo
        return (other.command == command)&&(other.method == method)
    }

    override fun hashCode(): Int {
        var result = command.hashCode()
        result = 31 * result + method.hashCode()
        return result
    }
}
