package ru.nsu.fit.avoloshina.websocketchat.model

data class Username(val username: String)