package ru.nsu.fit.avoloshina.websocketchat.client

import okhttp3.Response
import okhttp3.WebSocket
import okhttp3.WebSocketListener
import ru.nsu.fit.avoloshina.websocketchat.client.Client.Companion.DEBUG
import ru.nsu.fit.avoloshina.websocketchat.client.exceptions.*
import ru.nsu.fit.avoloshina.websocketchat.model.*


class ServerListener(private val client: Client) : WebSocketListener() {

    companion object {
        private const val NORMAL_CLOSURE_STATUS = 0
    }

    override fun onOpen(webSocket: WebSocket, response: Response) {
        client.statusHandler.handle(response.code())
        if (DEBUG) {
            println("WebSocket connected to server")
        }
    }

    override fun onMessage(webSocket: WebSocket, text: String) {
        if (text.contains("message")) {
            val responseMessage = client.gson.fromJson(text, ListElementMessage::class.java)
//            if (DEBUG) {
//                println("Received message:")
//                responseMessage.print()
//            }
            if (responseMessage.id != client.id) {
                client.addMessage(responseMessage)
                client.printMessage(responseMessage)
            }
        } else if (text.contains("users")) {
            val responseUsersList = client.gson.fromJson(text, UsersList::class.java)
                ?: throw UndefinedServerBehaviorException()
            client.addUsers(responseUsersList)

//            if (DEBUG) {
//                println("Received users")
//                responseUsersList.printUsers()
//            }
        }
    }

    override fun onClosed(webSocket: WebSocket, code: Int, reason: String) {
        webSocket.close(NORMAL_CLOSURE_STATUS, null)
    }

    override fun onFailure(webSocket: WebSocket, t: Throwable, response: Response?) {
        System.err.println("Server closed connection")
    }
}