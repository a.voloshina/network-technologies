package ru.nsu.fit.avoloshina.websocketchat.server

import com.google.gson.Gson
import io.undertow.server.HttpServerExchange
import ru.nsu.fit.avoloshina.websocketchat.model.*
import ru.nsu.fit.avoloshina.websocketchat.server.exceptions.UnknownTokenException
import ru.nsu.fit.avoloshina.websocketchat.server.exceptions.UnknownUserIdException
import ru.nsu.fit.avoloshina.websocketchat.server.exceptions.WrongRequestFormatException
import java.security.SecureRandom
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.CopyOnWriteArrayList

class ServerData {

    companion object {
        const val SUCCESS = 200
        private const val TIMER_REPEAT_TIME = 1000L
    }

//    var messagesListener: (MessagesList) -> Unit = { }
    var usersListener: (UsersList) -> Unit = { }

    val gson = Gson()
    private val secureRandom = SecureRandom()
    private val users = Users()
    private val messageHistory = MessagesList()
    val timedMessageHistory = ConcurrentHashMap<String, Long>()
    private val timer = Timer()
    private val responseUsersList = UsersList()

    init {
        //timer.schedule(TimeoutClients(this), 0, TIMER_REPEAT_TIME)
    }

    fun addUser(username: String): User {
        val bytes = ByteArray(20)
        secureRandom.nextBytes(bytes)
        val token = bytes.toString()

        for (user in users) {
            if (user.username == username) {
                user.online = true
                user.token = token
                usersListener(getUsers(user.token))
                println("User connected again:")
                user.print()
                users.printUsers()
                return user
            }
        }
        val user = User(users.size, username, true, token)
        users.add(user)
        usersListener(getUsers(user.token))
        println("Add new user:")
        user.print()
        users.printUsers()
        return user
    }

    fun addMessage(msg: ResponseMessage) {
        val userID = msg.id
        aliveUser(getUserTokenById(userID) ?: throw UnknownUserIdException(userID))
        messageHistory.messages.add(ListElementMessage(userID, msg.message, userID))
    }

    fun getUsers(token: String?): UsersList {
        if(token != null){
            aliveUser(token)
        }

        for (user in users) {
            val responseUser = ResponseUser(user.id, user.username, user.online)
            if(!responseUsersList.contains(responseUser)){
                responseUsersList.add(responseUser)
            }
        }

        return responseUsersList
    }

    fun getResponseUser(user: User): ResponseUser {
        return ResponseUser(user.id, user.username, user.online)
    }

    fun logoutUser(token: String) {
        for (user in users) {
            if (user.token == token) {
                println("User" + getUserIdByToken(token) + " logout")
                user.online = false
                usersListener(getUsers(null))
            }
        }
        users.printUsers()
    }

    fun deleteUser(token: String) {
        for (user in users) {
            if (user.token == token) {
                println("Timeout session for user " + user.id)
                user.online = null
                usersListener(getUsers(null))
                break
            }
        }
        users.printUsers()
    }

    private fun aliveUser(token: String) {
        val curTime = System.currentTimeMillis()
        val user = getUserByToken(token)
        user?.online = true
        timedMessageHistory[token] = curTime
    }

    fun getUserById(id: Int): User? {
        for (user in users) {
            if(user.id == id){
                return user
            }
        }
        return null
    }

    fun getUserIdByToken(token: String): Int? {
        for (user in users) {
            if (user.token == token) {
                return user.id
            }
        }
        return null
    }

    fun getUserStatusByToken(token: String): Boolean? {
        for (user in users) {
            if (user.token == token) {
                return user.online
            }
        }
        return null
    }

    fun getUserByToken(token: String): User? {
        for (user in users) {
            if (user.token == token) {
                return user
            }
        }
        return null
    }

    private fun getUserTokenById(id: Int): String? {
        for (user in users) {
            if (user.id == id) {
                return user.token
            }
        }
        return null
    }

    fun checkNewUsername(username: String): Boolean{
        for (user in users) {
            if ((user.username == username) && (user.online == true)) {
                return true
            }
        }
        return false
    }

    fun checkToken(token: String): Boolean{
        for (user in users) {
            if ((user.token == token) && (user.online == true)) {
                return true
            }
        }
        return false
    }

    fun checkAuthorizationHeader(exchange: HttpServerExchange): String{
        if (exchange.requestHeaders.contains("Authorization")) {
            return exchange.requestHeaders["Authorization"].first?.replace("Token ", "")
                ?: throw WrongRequestFormatException()
        } else {
            throw WrongRequestFormatException()
        }
    }

    fun checkContentTypeHeader(exchange: HttpServerExchange){
        if (exchange.requestHeaders.contains("Content-Type")) {
            if(!exchange.requestHeaders["Content-Type"].first.contains("application/json")){
                throw WrongRequestFormatException()
            }
        } else {
            throw WrongRequestFormatException()
        }
    }

}
