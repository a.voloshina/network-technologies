package ru.nsu.fit.avoloshina.websocketchat.model

data class ResponseUser(val id: Int,
                        val username: String,
                        var online: Boolean?) {

    override fun equals(other: Any?): Boolean {
        if(other !is ResponseUser) {
            return false
        }
        return username == other.username && id == other.id
    }

    override fun hashCode(): Int {
        var result = id
        result = 31 * result + username.hashCode()
        return result
    }

    fun print() {
        println("{")
        println("   id: $id,")
        println("   username: $username")
        println("   online: $online")
        println("}")
    }
}
