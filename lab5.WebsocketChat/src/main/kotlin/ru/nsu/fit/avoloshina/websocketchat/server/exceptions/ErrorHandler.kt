package ru.nsu.fit.avoloshina.websocketchat.server.exceptions

import io.undertow.server.HttpServerExchange
import io.undertow.util.HttpString

class ErrorHandler() {

    enum class ErrorType(val type: Int){
        WRONG_REQUEST_FORMAT(400),
        NO_TOKEN(401),
        BUSY_USERNAME(401),
        UNKNOWN_TOKEN(403),
        UNKNOWN_USER_ID(404),
        UNSUPPORTED_METHOD(405),
        EXCEPTION(500)
    }

    companion object {

        fun handleError(code: ErrorType, exchange: HttpServerExchange) {
            if (code == ErrorType.BUSY_USERNAME) {
                exchange.responseHeaders.add(HttpString("WWW-Authenticate"), "Token realm='Username is already in use'")
            }
            exchange.statusCode = code.type
            exchange.responseSender.send("")
        }

    }

}
