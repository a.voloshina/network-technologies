package ru.nsu.fit.avoloshina.websocketchat.server.handlers

import io.undertow.server.HttpHandler
import io.undertow.server.HttpServerExchange
import io.undertow.util.HttpString
import ru.nsu.fit.avoloshina.websocketchat.server.exceptions.UnknownTokenException
import ru.nsu.fit.avoloshina.websocketchat.server.exceptions.UnknownUserIdException
import ru.nsu.fit.avoloshina.websocketchat.server.exceptions.WrongRequestFormatException
import ru.nsu.fit.avoloshina.websocketchat.server.ServerData

class GetUsersHandler(private val serverData: ServerData) : HttpHandler {
    override fun handleRequest(exchange: HttpServerExchange) {
        val f = exchange.requestReceiver.receiveFullBytes { _, _ ->
            val token = serverData.checkAuthorizationHeader(exchange)
            print("User $token ask for user")
            if(serverData.checkToken(token)){
                val splitPath = exchange.relativePath.split("/")
                exchange.responseHeaders.add(HttpString("Content-Type"), "application/json")
                exchange.statusCode = ServerData.SUCCESS
                if (splitPath.size <= 2) {
                    val usersList = serverData.getUsers(token)
                    println("s list:")
                    usersList.printUsers()
                    exchange.responseSender.send(serverData.gson.toJson(usersList))
                }
                else {
                    val userID = try {
                        splitPath[2].toInt()
                    } catch (ex: NumberFormatException) {
                        throw WrongRequestFormatException()
                    }
                    val user = serverData.getUserById(userID)
                    if(user != null){
                        println(" $userID:")
                        user.print()
                        exchange.responseSender.send(serverData.gson.toJson(user))
                    } else {
                        println()
                        throw UnknownUserIdException(userID)
                    }
                }
            } else {
                println()
                throw UnknownTokenException(token)
            }
        }
    }
}
