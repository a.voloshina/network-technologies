package ru.nsu.fit.avoloshina.websocketchat.client.exceptions

import ru.nsu.fit.avoloshina.websocketchat.client.Client

class StatusHandler(private val client: Client) {

    enum class ErrorType(val type: Int){
        SUCCESS(200),
        WRONG_REQUEST_FORMAT(400),
        NO_TOKEN(401),
        BUSY_USERNAME(401),
        UNKNOWN_TOKEN(403),
        UNKNOWN_USER_ID(404),
        UNSUPPORTED_METHOD(405),
        EXCEPTION(500)
    }

    fun handle(code: Int): Boolean{
        when(code){
            ErrorType.SUCCESS.type -> return true
            ErrorType.BUSY_USERNAME.type -> {
                println("Username is busy. Choose other nickname.")
                client.login()
            }
            ErrorType.UNKNOWN_TOKEN.type -> {
                client.getToken(client.username)
            }
            ErrorType.UNKNOWN_USER_ID.type -> {
                System.err.println("Unknown user id")
            }
            ErrorType.WRONG_REQUEST_FORMAT.type -> {
                System.err.println("Wrong command format!")
            }
        }
        return false
    }

}
