package ru.nsu.fit.avoloshina.websocketchat.model

class ListElementMessage(val id: Int,
                         val message: String,
                         val author: Int) {

    fun print() {
        println("{")
        println("   id: $id")
        println("   message: $message")
        println("   author: $author")
        println("}")
    }

}
