package ru.nsu.fit.avoloshina.websocketchat.model

import java.util.concurrent.CopyOnWriteArrayList

data class UsersList(private val users: CopyOnWriteArrayList<ResponseUser> = CopyOnWriteArrayList()) : Iterable<ResponseUser> {
    val size: Int
        get() = users.size

    fun add(user: ResponseUser) {
        users.add(user)
    }

    fun get(index: Int): ResponseUser {
        return users[index]
    }

    fun setStatus(index: Int, status: Boolean?){
        users[index].online = status
    }

    override fun iterator() = users.iterator()

    fun getUsernameById(id: Int): String? {
        for (user in users) {
            if(user.id == id){
                return user.username
            }
        }
        return null
    }

    fun printUsers() {
        println("\"users\": [")
        for (user in users) {
            user.print()
            println()
        }
        println("]")
    }

}
