package ru.nsu.fit.avoloshina.websocketchat.server.exceptions

class NoTokenException: ServerException(ErrorHandler.ErrorType.NO_TOKEN) {
}