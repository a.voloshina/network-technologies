package ru.nsu.fit.avoloshina.websocketchat.server.exceptions

import io.undertow.util.HttpString

class UnsupportedMethodException(private val command: String,
                                 private val method: HttpString): ServerException(ErrorHandler.ErrorType.UNSUPPORTED_METHOD)