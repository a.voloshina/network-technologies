package ru.nsu.fit.avoloshina.websocketchat.server.handlers

import io.undertow.server.HttpHandler
import io.undertow.server.HttpServerExchange
import io.undertow.util.HttpString
import ru.nsu.fit.avoloshina.websocketchat.model.RequestMessage
import ru.nsu.fit.avoloshina.websocketchat.server.ServerData
import ru.nsu.fit.avoloshina.websocketchat.server.exceptions.ErrorHandler

class LogoutHandler(private val serverData: ServerData): HttpHandler {
    override fun handleRequest(exchange: HttpServerExchange) {
        val f = exchange.requestReceiver.receiveFullBytes { _, _ ->
            val token = serverData.checkAuthorizationHeader(exchange)
            if(serverData.checkToken(token)){
                exchange.responseHeaders.add(HttpString("Content-Type"), "application/json")
                exchange.statusCode = ServerData.SUCCESS
                exchange.responseSender.send(serverData.gson.toJson(RequestMessage("bye!")))
                println("User logout: ")
                serverData.getUserByToken(token)?.print()
                serverData.logoutUser(token)
            } else {
                ErrorHandler.handleError(ErrorHandler.ErrorType.UNKNOWN_TOKEN, exchange)
            }
        }
    }
}
