package ru.nsu.fit.avoloshina.websocketchat.server.exceptions

class UnknownSituationException: ServerException(ErrorHandler.ErrorType.EXCEPTION)