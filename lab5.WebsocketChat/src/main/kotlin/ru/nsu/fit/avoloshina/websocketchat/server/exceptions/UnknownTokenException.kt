package ru.nsu.fit.avoloshina.websocketchat.server.exceptions

class UnknownTokenException(private val token: String): ServerException(ErrorHandler.ErrorType.UNKNOWN_TOKEN)