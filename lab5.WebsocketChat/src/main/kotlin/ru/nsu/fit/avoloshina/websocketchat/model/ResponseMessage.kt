package ru.nsu.fit.avoloshina.websocketchat.model

data class ResponseMessage(val id: Int,
                           val message: String){

    fun print() {
        println("{")
        println("   id: $id")
        println("   message: $message")
        println("}")
    }

}
