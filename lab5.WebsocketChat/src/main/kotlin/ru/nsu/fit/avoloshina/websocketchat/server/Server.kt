package ru.nsu.fit.avoloshina.websocketchat.server

import io.undertow.Handlers
import io.undertow.Handlers.path
import io.undertow.Undertow
import io.undertow.server.handlers.PathHandler
import io.undertow.websockets.core.AbstractReceiveListener
import io.undertow.websockets.core.BufferedTextMessage
import ru.nsu.fit.avoloshina.websocketchat.model.ListElementMessage
import ru.nsu.fit.avoloshina.websocketchat.server.handlers.*
import io.undertow.websockets.core.WebSockets
import io.undertow.websockets.core.WebSocketChannel
import ru.nsu.fit.avoloshina.websocketchat.model.ResponseMessage

class Server(serverIP: String, serverPort: Int) {

    companion object {
        const val DEBUG = true
    }

    private val data = ServerData()

    private val pathHandler = PathHandler(
        path()
            .addPrefixPath("login", LoginHandler(data))
            .addPrefixPath("logout", LogoutHandler(data))
            .addPrefixPath("users", GetUsersHandler(data))
            .addPrefixPath("messages", Handlers.websocket{ exchange, channel ->

                println("Send users:")

                data.usersListener = {
                        users ->
                    println("Send users:")
                    users.printUsers()
                    val responseUsersList = data.gson.toJson(users)
                    for (session in channel.peerConnections) {
                        WebSockets.sendText(responseUsersList, session, null)
                    }
                }

                channel.receiveSetter.set(object: AbstractReceiveListener(){
                    override fun onFullTextMessage(channel: WebSocketChannel, message: BufferedTextMessage?) {
                        val text = message?.data
                        val requestMessage = data.gson.fromJson(text, ResponseMessage::class.java)
                        if (requestMessage != null) {
                            if(DEBUG){
                                println("Received message:")
                                requestMessage.print()
                            }
                            data.addMessage(requestMessage)
                            val responseMessage = data.gson.toJson(ListElementMessage(requestMessage.id,
                                requestMessage.message, requestMessage.id))
                            for (session in channel.peerConnections) {
                                WebSockets.sendText(responseMessage, session, null)
                            }
                        }
                    }
                })
                channel.resumeReceives()
            })
    )

    private val server = Undertow.builder()
        .addHttpListener(serverPort, serverIP, pathHandler)
        .build()

    fun start() {
        server.start()
    }

    fun stop() {
        server.stop()
    }

}
